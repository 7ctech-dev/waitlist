import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router} from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  
  showBackBtn:any;
  hideCross:any;
  fp_waitlist_step:any;
  lastOrderCheck:any;
  checkCustomerPromo:any;
  constructor(
    private cookieService: CookieService,
    private data:DataService,
    private router: Router
  ) { }

  ngOnInit() {
    this.hideCross = "false";
    this.lastOrderCheck = 'false';
    this.checkCustomerPromo = 'false';
    if(this.cookieService.get('fp_waitlist_step') == "priority-list"){
        this.hideCross = "true";
    }
    if(this.cookieService.get('fp_waitlist_step') == "purchase-plan"){
      this.fp_waitlist_step = 1;
    }else{
      this.fp_waitlist_step = 0;
    }
    window.scrollTo(0, 0)
    if(this.cookieService.get('fp_waitlist_step') == 'purchase-plan'){
      this.showBackBtn = true;
    }else {
      this.showBackBtn = false;
    }
    if(this.cookieService.get('fp_waitlist_last_order')){
      this.lastOrderCheck = 'true';
    }
    if(this.cookieService.get('fp_waitlist_checkCustomerPromo') == 'true'){
      this.checkCustomerPromo = 'true';
    }else{
      this.data.checkCustomerPromo().subscribe(data => {
        this.cookieService.set('fp_waitlist_checkCustomerPromo', data);
        if(data == 'true')
        this.checkCustomerPromo = 'true';
        else
        this.checkCustomerPromo = 'false';
      })
    }
    var hostname = window.location.origin;
    if(hostname.indexOf("stage") == -1 && hostname.indexOf("localhost") == -1){
      let number = this.cookieService.get("planSpotsRamdomNumber")
      if(number == ''){
        number = this.data.getRandomIntInclusive(6,9).toString()
       //  this.cookieService.set("planSpotsRamdomNumber",number.toString());
        document.cookie = "planSpotsRamdomNumber="+number+";domain=.thefashionpass.com";
      }
      this.data.planSpotRemainingFunc(number)
    }
  }
  gotoPlans(){
    this.cookieService.delete('fp_waitlist_selected_plan');
    this.cookieService.set( 'fp_waitlist_step',  "plans" );
    this.router.navigate(['plans']);
  }
  
  gotoMainSite(){
    this.data.gotoMainSite();
}
  reset_waitlist(){
    
  }
}
