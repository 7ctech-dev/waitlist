import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewWaitlistComponent } from './new-waitlist.component';

describe('NewWaitlistComponent', () => {
  let component: NewWaitlistComponent;
  let fixture: ComponentFixture<NewWaitlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewWaitlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewWaitlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
