import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any;

@Component({
  selector: 'app-new-waitlist',
  templateUrl: './new-waitlist.component.html',
  styleUrls: ['./new-waitlist.component.css']
})
export class NewWaitlistComponent implements OnInit {

  EmailForm:FormGroup;
  vip_wt_form:FormGroup;
  email:string;
  expandedError:any;
  invalidEmailError:any;
  showError:any;
  showErrorVip:any;
  showValidationError:any;
  showValidationErrorVip:any;
  invalidEmailErrorVip:any;
  selectedEmail:any;
  ApplyBlackFriday:any;
  waitTime:string;
  check30Credit: any;
  constructor(
    private cookieService: CookieService,
    private data:DataService, 
    private router: Router,
    private http:HttpClient,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) { }

 myEmail:string;
  ngOnInit() {
    this.showValidationError = false;
    this.showError = false;
    this.showErrorVip = false;
    this.showValidationErrorVip = false;
    this.invalidEmailErrorVip = false;
    this.selectedEmail = '';
    if(window.innerWidth > 767) {
    document.getElementById("email").focus();}
    //this.spinner.show();
    //setTimeout(() => { this.spinner.hide(); }, 5000);
    
    //BLACK FRIDAY
    if(this.data.BLACK_FRIDAY_SALE == true){
      this.ApplyBlackFriday = true;
    }else{
      this.ApplyBlackFriday = false;
    }

    this.data.checkPageView(); 
    this.expandedError = false;
    this.invalidEmailError = false;

    if(this.cookieService.get('fp_waitlist_check_email')){
        this.spinner.show();
        this.sendEmailRequest(this.cookieService.get('fp_waitlist_check_email'));
    }else{
      new setFBPixel();
    }
    const email_regex = '^([a-zA-Z0-9_\.\-]{2,})+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$'; 
    this.EmailForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email, Validators.pattern(email_regex), firstLetterCheck]],
    })

    this.vip_wt_form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern(email_regex), firstLetterCheck]],
    })

  if(this.cookieService.get('fp_waitlist_emailSelect')){
        this.selectedEmail = this.cookieService.get('fp_waitlist_emailSelect');
        this.EmailForm.setValue({
          email:this.selectedEmail
        });
        this.vip_wt_form.setValue({
          email:this.selectedEmail
        });

    }
    // var postUrlBanjo = "interactions/getWaitlistTime";              
      this.data.getWaitlistTIme()
       .subscribe(data => {
         console.log(data);
         this.waitTime = data;
         this.cookieService.set( 'wt_hour',  data);
       })
      // this.waitTime = "72 HRS";
      this.data.check30CreditOn().subscribe(data => {
        console.log(data +" check $30 credit ====-------");
        this.check30Credit = data;
        this.cookieService.set('fp_waitlist_check30CreditOn', data);
      })
  }

  checkError(){
    this.expandedError = false;
    this.invalidEmailError = false;
  }
  hideError(){
    this.showValidationError = false;
    this.expandedError = false;
    this.invalidEmailError = false;
    this.showValidationErrorVip = false;
    this.invalidEmailErrorVip = false;
  }
  gotoCompleteProfile() {
    this.router.navigate(['complete-profile']);
  }
  submitVIPCustomer(){
    if (this.vip_wt_form.valid) { 
      /*if(this.cookieService.get('fp_waitlist_customer_type')){
          this.cookieService.delete('fp_waitlist_customer_type');
      }*/
      this.cookieService.set("fp_waitlist_customer_type", "vip");
      this.invalidEmailErrorVip = false;
      this.expandedError = false;
      this.spinner.show();
      this.email = this.vip_wt_form.value.email;
      this.sendEmailRequest(this.email);
    }else{
      this.showErrorVip = true;
      this.showValidationErrorVip = true;
      this.invalidEmailErrorVip = false;
    }
  }

  submitEmailAddress(){
    if (this.EmailForm.valid) {
      this.cookieService.set("fp_waitlist_customer_type", "general");
      this.invalidEmailError = false;
      this.expandedError = false;
      this.spinner.show();
      this.email = this.EmailForm.value.email;
      this.sendEmailRequest(this.email);
    }else{
      this.showError = true;
      this.showValidationError = true;
      this.invalidEmailError = false;
    }
  }
  
  sendEmailRequest(customer_email){
    
    this.email = customer_email;
    let _this = this;
    let FormData = "email="+customer_email;
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.data.DBINFOURL+"Waitlist/checkWaitlistEmail/", FormData , { responseType: 'text', headers: headerOptions })
    .subscribe(
        data => {
            var fp_waitlist_selected_plan = '';
            var fp_waitlist_customer_type = '';
            if(_this.cookieService.get('fp_waitlist_selected_plan_web')){ //&&  logedInUser == "true"
                fp_waitlist_selected_plan =  _this.cookieService.get('fp_waitlist_selected_plan_web');
            } 
            if(_this.cookieService.get('fp_waitlist_customer_type')){
              fp_waitlist_customer_type = _this.cookieService.get('fp_waitlist_customer_type');
            }
            
             if(data == "true"){ 
                var expire = new Date();
                //var time = Date.now() + ((3600 * 1000) * 6); // current time + 6 hours ///
                expire.setTime(expire.getTime()+(30*24*60*60*1000));
                _this.data.reset_waitlist_cookies();
                if(fp_waitlist_selected_plan!=''){
                  _this.cookieService.set('fp_waitlist_selected_plan', fp_waitlist_selected_plan);
                }
                if(fp_waitlist_customer_type!=''){
                  _this.cookieService.set('fp_waitlist_customer_type',fp_waitlist_customer_type);
                }
                _this.cookieService.set( 'fp_waitlist_email',  _this.email, expire);
                _this.cookieService.set( 'fp_waitlist_step',  "complete-profile" );
                _this.router.navigate(['complete-profile']);
                // document.location.href = '/complete-profile';
             }else if(data == "false"){
              _this.expandedError = true;
             }else if(data == "invalid"){
                if(_this.cookieService.get('fp_waitlist_check_email')){
                  _this.EmailForm.setValue({email:_this.cookieService.get('fp_waitlist_check_email')});
                }
                if(fp_waitlist_customer_type=="vip"){
                  _this.invalidEmailErrorVip = true;
                }else{
                  _this.invalidEmailError = true;
                }
                _this.spinner.hide();
             }else if(data.indexOf("shopifysuer") !=  -1){
                  var splitData = data.split(":");
                  var customerAtemp = splitData[1];
                  _this.cookieService.delete('fp_waitlist_check_email');
                  _this.cookieService.set('fp_waitlist_attempt_customer', customerAtemp);
                  window.location.href =  _this.data.SHOPIFY_FRONTEND_URL+"?openlogin="+btoa(_this.email);
             }
             else{
               var black_sale_pause_customer = "";
                if(_this.cookieService.get('black_friday_pause_customer')){
                    black_sale_pause_customer ="PauseCustomer";
                }
                var plansSpotsLeft = '';
                if(_this.cookieService.get('plansSpotsLeft')){
                  plansSpotsLeft = _this.cookieService.get('plansSpotsLeft');
                }
                _this.data.reset_waitlist_cookies();
                if(black_sale_pause_customer.length > 0){
                  _this.cookieService.set('black_sale_pause_customer', black_sale_pause_customer);
                }
                if(fp_waitlist_selected_plan!=''){
                  _this.cookieService.set('fp_waitlist_selected_plan', fp_waitlist_selected_plan);
                }
                if(fp_waitlist_customer_type!=''){
                  _this.cookieService.set('fp_waitlist_customer_type',fp_waitlist_customer_type);
                }
                if(plansSpotsLeft != ''){
                  _this.cookieService.set('plansSpotsLeft', plansSpotsLeft);
                }
                _this.cookieService.delete('fp_waitlist_check_email');
                var responseData = JSON.parse(data);
                if(responseData.customerTags){
                  if(responseData.customerTags.indexOf("bad_customer") !=  -1 && responseData.customerTags.indexOf("NeverBlocked") ==  -1){
                    _this.cookieService.set('fp_waitlist_bad_customer','BAD_CUSTOMER');
                       
                  } 
                }
                console.log(responseData);
                
                    if(responseData.LastOrderID){
                      _this.cookieService.set('fp_waitlist_last_order', responseData.LastOrderID);
                    }
                    if((responseData.customer_shopify_id =='' || responseData.customer_shopify_id ==null || responseData.customer_stripe_id ==''  ||  responseData.customer_stripe_id ==null) &&  responseData.status != 'active' ){
                          var shopifyID = responseData.customer_shopify_id;
                          var stripeID = responseData.customer_stripe_id;
                          _this.cookieService.set( 'fp_waitlist_email',  _this.email );
                          _this.cookieService.set('fp_waitlist_step',"complete-profile");
                          _this.router.navigate(['complete-profile']);
                      //    this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                    }
                    else if((responseData.selected_plan !=''  && responseData.selected_plan !=null && responseData.selected_plan.length >0 && _this.waitTime != '0') ||  responseData.status == 'active' ){
                      _this.cookieService.set('fp_waitlist_email',  _this.email);
                          _this.cookieService.set('CustomerStripeID',  btoa(responseData.customer_stripe_id));
                          _this.cookieService.set('CustomerShopifyID', btoa(responseData.customer_shopify_id));  
                          _this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                          _this.cookieService.set('fp_waitlist_selected_plan',responseData.selected_plan);
                          _this.cookieService.set('fp_waitlist_step',"priority-list");
                          _this.router.navigate(['priority-list']);
                          /*window.location.href =  this.data.SHOPIFY_FRONTEND_URL+"?openlogin="+btoa(this.email);*/
                    }else{
                      _this.cookieService.set('fp_waitlist_email',  _this.email);
                          _this.cookieService.set('CustomerStripeID',  btoa(responseData.customer_stripe_id));
                          _this.cookieService.set('CustomerShopifyID', btoa(responseData.customer_shopify_id));  
                          _this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                          if(responseData.oldCustomer){
                            _this.cookieService.set('fp_waitlist_oldCustomer', "true");
                          }

                          _this.data.getCustomerStripeCard().subscribe(
                            data  => {
                                if(data){
                                  var previousCard  = "XXXX XXXX XXXX "+data[0];
                                  if(_this.cookieService.get('fp_waitlist_previousCard')){
                                    _this.cookieService.delete('fp_waitlist_previousCard');
                                  }
                                  _this.cookieService.set('fp_waitlist_previousCard', previousCard+","+data[1]+","+data[2]+","+data[3]);
                                }else{}
                            },
                            error => {
                                console.log("Card Error", error);
                            });

                          var logedInUser = "false";
                          if(_this.cookieService.get('fp_waitlist_loggedin_customer')){
                              logedInUser = "true";
                          }

                          if(_this.cookieService.get('fp_waitlist_selected_plan')){ //&&  logedInUser == "true"
                            setTimeout(() => {
                              _this.cookieService.set('fp_waitlist_step',"purchase-plan");
                              _this.router.navigate(['purchase-plan']);
                            }, 1000);
                          }
                          /*else if(this.cookieService.get('fp_waitlist_step')){

                          }*/
                          else{
                            if(localStorage.getItem("fp_waitlist_email_type")){
                              _this.cookieService.set('fp_waitlist_email_type',localStorage.getItem("fp_waitlist_email_type"));
                              var obj = {"email":_this.cookieService.get('fp_waitlist_email'),"email_type":_this.cookieService.get('fp_waitlist_email_type'),"event":'viewed_select_plan'};
                              console.log(obj);
                              _this.data.setUserActivityBanjo(obj).subscribe(data => {
                                console.log('data responce'+data)
                              });
                            }
                            _this.cookieService.set('fp_waitlist_step',"plans");
                            _this.router.navigate(['plans']);
                                /*if(this.cookieService.get('fp_waitlist_oldCustomer')){
                                    this.cookieService.set('fp_waitlist_step',"thankyou");
                                    this.router.navigate(['thankyou']);
                                }else{ }*/
                          }
                    }
                  //console.log();
             }
             //this.spinner.hide();
        },
        error => {
            console.log("Error", error);
        }
    );
    
  }

} 
function firstLetterCheck(control: FormControl) {
  let data = control.value;
  if(data.length > 0){
    var firstLetter = data.substr(0,1);
    console.log(firstLetter);
    if(firstLetter == "." || firstLetter == "_" || firstLetter == "-"){
      return {
        CheckFirstLetter:{
          chk:true
        }
      }
    }
    return null;
  }
  return null;
}
