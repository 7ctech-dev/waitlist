import { Component, OnInit, ViewEncapsulation, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import {  CountdownComponent  } from 'ngx-countdown';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any; 

@Component({
  selector: 'app-thankyou',
  templateUrl: './thankyou.component.html',
  styleUrls: ['./thankyou.component.css']
})
export class ThankyouComponent implements OnInit {
  @ViewChild('countdown') counter: CountdownComponent;
  constructor(
    private data:DataService, 
    private router: Router,
    private cookieService: CookieService, 
   // private counter:CountdownComponent 
    ) { }
  lastOrderCheck:any;
  ApplyBlackFriday:any;
  ngOnInit() {
    new setFBPixel();
    this.data.checkPageView();
    this.lastOrderCheck = 'false';
    if(this.cookieService.get('fp_waitlist_last_order')){
      this.lastOrderCheck = 'true';
    }
     //BLACK FRIDAY
     if(this.data.BLACK_FRIDAY_SALE == true){
      this.ApplyBlackFriday = true;
    }else{
      this.ApplyBlackFriday = false;
    }
  }
  gotoPlans() {
    let countdown = document.getElementsByTagName('countdown')[0];
    this.cookieService.set('countdown_timer', countdown.textContent);
    
    if(this.cookieService.get('fp_waitlist_selected_plan')){
        this.cookieService.set( 'fp_waitlist_step',  "purchase-plan" );
        this.router.navigate(['purchase-plan']);
    }else {
        this.cookieService.set( 'fp_waitlist_step',  "plans" );
        this.router.navigate(['plans']);
    }
  }
  
  resetTimer(){
    this.counter.restart();
  }
  showFinishMessage(){
    //this.counter.restart();
    setTimeout(() => this.counter.restart());
  }
}
