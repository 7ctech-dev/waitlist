import { Component, OnInit, ViewChild,ViewChildren, ElementRef  } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Router,NavigationEnd } from '@angular/router';
import { DataService } from '../data.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {  CountdownComponent  } from 'ngx-countdown';
import { parse } from 'querystring';
import { last } from '@angular/router/src/utils/collection';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any;
declare var gtag_report_conversion: any;
 const now = new Date();
@Component({
  selector: 'app-purchase-plan',
  templateUrl: './purchase-plan.component.html',
  styleUrls: ['./purchase-plan.component.css','./purchase-plan.component.scss']
})

export class PurchasePlanComponent implements OnInit {
  @ViewChild('countdown') counter: CountdownComponent;
  @ViewChild('referral_code') refCodeInput: ElementRef;
  constructor(
    private cookieService: CookieService,
    private http:HttpClient,
    private router: Router,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService,
    private data:DataService
    
  ) { }

  PurchasePlanForm: FormGroup;
  
  currentPlaneName        :string;
  currentPlanePrice       :string;
  currentPlaneDescription :string;
  PriorityDiscount        :any;
  name_on_card            :string;
  card_number             :any;
  card_expiry             :number;
  card_cvv                :number;
  zip_code                :string;
  term_agree              :string;
  cardError               :string;
  CardErrorMessage        :string;
  FirstMonthPrice         :any;
  res         :any;
  current_month : any;
  fp_authorize :any;
  showBackBtn:any;
  customer_fullname : string;
  refDiscount:any;
  acountCredit:any;
  promoCredit:any;
  promoCreditDiscount:any;
  cardName : string;
  cardImage : string;
  validRefCode:string;
  validGiftCode:string;
  validPromoCode:string;
  showRefError:string;
  showValidationError:string;
  referral_code : string;
  validZipCode :string;
  showCheckoutBox:any;
  showBorder1:any;
  showBorder2:any;
  showTermOfService:any;
  city : string;
  state: string;
  phone_number:any;
  phone_number_error:string;
  hasPreviousCard:any;
  previousCard:string;
  ApplyFreeCoupon:any;
  counterLeftTime: any;
  LastOrderCheck:any;
  ApplyBlackFriday:any;
  waitTime:string;
  waitTimeHours:number
  customerDetail:any;
  checkUserCreate:any;
  waitTimeUthorize:any;
  requestPending:any;
  couponAmount_type:any;
  promoCodeObj:any;
  check30Credit:any;
  waitlistCusRes:any;
  showGiftCode:string;
  promoCodeData:any;
  giftCodeData:any;
  validPromoCodeMessage:string;
  dynemicCreditForCoupon:number;
  invalidGiftCodeMessage:string;
  creditForMembers : number;
  promoCodeAllowFor : string;
  checkGiftReq : boolean;
  promoRefButton: boolean;
  giftButton: boolean;
  plansSpotsLeft:any;
  ngOnInit() {   
    setTimeout(() => new setFBPixel(),1000);
    this.LastOrderCheck = 'false';
    var newTime = 300;
    if(this.cookieService.get("countdown_timer")){
        var countdown_timer = this.cookieService.get("countdown_timer");
        var split_timer = countdown_timer.split(':');
        newTime =  (parseInt(split_timer[0])*60)+parseInt(split_timer[1]);
        this.cookieService.delete("countdown_timer");
    }
    this.counterLeftTime = newTime;
    this.ApplyFreeCoupon = false;
    this.previousCard = '';
    this.hasPreviousCard = false;
    this.showTermOfService = false;
    this.showCheckoutBox= false;
    this.showBackBtn = true;
    this.data.checkPageView();
    this.cardError        = "false";
    this.CardErrorMessage = "";
    this.showRefError = 'false';
    this.cardName = '';
    this.cardImage = '';
    this.validRefCode ='';
    this.promoRefButton = true;
    this.giftButton = true;
    this.validGiftCode = "";
    this.showBorder1 = false;
    this.showBorder2 = false;
    this.validZipCode = "";
    this.showValidationError = "false";
    this.referral_code = '';
    this.city = '';
    this.state = '';
    this.phone_number = '';
    this.phone_number_error = 'false';
    this.showGiftCode = 'false';
    this.dynemicCreditForCoupon = 0;
    this.checkGiftReq = false;
    this.plansSpotsLeft = {socialite:4,trendsetter:1,wanderlust:2}
    if(this.cookieService.get("plansSpotsLeft")){
      this.plansSpotsLeft = JSON.parse(this.cookieService.get("plansSpotsLeft"));
      console.log(this.plansSpotsLeft)
    }else{
      console.log('here i m test')
    }
    
    // const expiryRegex = '^(((0)[1-9])|((1)[0-2]))(19|2[0-9]|90)$';
    const expiryRegex = '^(((0)[1-9])|((1)[0-2]))(2[0-9])$';

    //BLACK FRIDAY
    if(this.data.BLACK_FRIDAY_SALE == true){
      this.ApplyBlackFriday = true;
    }else{
      this.ApplyBlackFriday = false;
    }
    this.cookieService.delete('fp_waitlist_promoCode');
    this.PurchasePlanForm = this.formBuilder.group({
      name_on_card: ['', [Validators.required, Validators.pattern('^[a-zA-Z\\s]+$'), noWhitespaceValidator]],
      card_number: ['', [Validators.required, Validators.minLength(13), Validators.maxLength(16), Validators.pattern('^[0-9]+$')]],
      card_expiry:['',[Validators.required,Validators.maxLength(4), Validators.pattern(expiryRegex), CardExpiryValidator]], //
      zip_code:['', [Validators.required, Validators.pattern('^[0-9]+$')]],
      card_cvv:['', [Validators.required,  Validators.pattern('^[0-9]+$')]],
      term_agree: ['', Validators.required],
      fp_authorize : ['', Validators.required],
      credit_30 : ['', Validators.required],
      phone_number : ['', [Validators.required, firstLetterCheck]],
      ref_code : [''],
      gift_code : ['']  
    });
    this.data.getWaitlistTIme()
    .subscribe(data => {
      console.log(data);
      if(data.indexOf('HRS') > -1){
        this.waitTimeHours = parseInt(data.split(' ')[0])
        // console.log(data.split(' '))
     }else{
      this.waitTimeHours = 0;
     }
      this.waitTime = data;
      if(this.waitTime  == '0'){
        this.PurchasePlanForm.controls['fp_authorize'].disable()
      }
      this.cookieService.set( 'wt_hour',  data);
    })
    // if(this.cookieService.get('fp_waitlist_check30CreditOn')){
    //   this.check30Credit = this.cookieService.get('fp_waitlist_check30CreditOn');
    // }else{
      // this.data.check30CreditOn().subscribe(data => {
      //   console.log(data +" check $30 credit ====-------");
      //   this.cookieService.set('fp_waitlist_check30CreditOn', data);
      //   this.check30Credit = data; 
      //   if(this.check30Credit == '0'){
      //     this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,'0');
      //   }
        
        
      //   })
        // this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,'0');
    // }
    if(this.cookieService.get("fp_waitlist_last_order")){
      this.LastOrderCheck = 'true';
      // this.PurchasePlanForm.controls['ref_code'].disable();
    }
    this.data.getWaitlistCredit(this.cookieService.get('fp_waitlist_email')).subscribe(
        data  => {  
            const usersJson: any[] = Array.of(data);
            var response = usersJson[0];
            if(response[2] % 1 != 0){
                if(response[2]){
                  this.acountCredit = parseFloat(response[2]); 
                }
                if(response[1]){
                  this.refDiscount =  parseFloat(response[1]); 
                }
                if(response[0]){
                  this.PriorityDiscount = 0;
                  this.creditForMembers = parseFloat(response[0]);
                  // console.log('this credit for member '+this.creditForMembers)
                } 
            }else{
                if(response[2]){
                  this.acountCredit = parseInt(response[2]); 
                }
                if(response[1]){
                  this.refDiscount =  parseInt(response[1]); 
                }
                if(response[0]){
                  this.PriorityDiscount = 0;
                  this.creditForMembers = parseFloat(response[0]);
                  // console.log('this credit for member '+this.creditForMembers)
                } 

            }
            // this.creditForMembers = 30;
            var creditForMember;
            
            if(this.cookieService.get("fp_waitlist_last_order")){
              this.creditForMembers = 0;
            }
            creditForMember = this.creditForMembers;
            this.cookieService.set('creditForMembers', creditForMember.toString());
            console.log(this.creditForMembers+' credit for new user -------')

            this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,'0'); 
            this.showCheckoutBox = true;
      },
      error => {
          console.log("Error", error);
      }
    ); 
 
        if(this.cookieService.get('fp_waitlist_apply_code') ){
          this.PurchasePlanForm.setValue({
            name_on_card:this.cookieService.get('fp_waitlist_fullname'),
            card_number:this.cookieService.get('fp_waitlist_previousCard'),
            card_expiry:'',
            zip_code:'',
            card_cvv :'',
            term_agree :'',
            fp_authorize :'',
            phone_number:'',
            credit_30 : '',
            ref_code : this.cookieService.get('fp_waitlist_apply_code'),
            gift_code: ''
          });
          this.checkRefCode(this.cookieService.get('fp_waitlist_apply_code'));
        }else{
            this.PurchasePlanForm.setValue({
              name_on_card:this.cookieService.get('fp_waitlist_fullname'),
              card_number:this.cookieService.get('fp_waitlist_previousCard'),
              card_expiry:'',
              zip_code:'',
              card_cvv :'',
              term_agree :'',
              fp_authorize :'',
              phone_number:'',
              credit_30 : '',
              ref_code :'',
              gift_code:''
            });
        }        
       
        if(!this.cookieService.get('fp_waitlist_previousCard')){
          this.data.getCustomerStripeCard().subscribe(
          data  => {
              if(data){
                var previousCard  = "XXXX XXXX XXXX "+data[0];
                if(this.cookieService.get('fp_waitlist_previousCard')){
                    this.cookieService.delete('fp_waitlist_previousCard');
                }
                this.cookieService.set('fp_waitlist_previousCard', previousCard+","+data[1]+","+data[2]+","+data[3]);
                if(this.cookieService.get('fp_waitlist_previousCard')){
                  var data = this.cookieService.get('fp_waitlist_previousCard');
                  console.log(data);
                  var exp = data.split(",");
                  this.previousCard = exp[0];
                  this.hasPreviousCard = true;
                  this.PurchasePlanForm.setValue({
                    name_on_card:this.cookieService.get('fp_waitlist_fullname'),
                    card_number:exp[0],
                    card_expiry:exp[2],
                    zip_code:exp[1],
                    card_cvv :'XXXX',
                    term_agree :'',
                    fp_authorize :'',
                    phone_number:'',
                    credit_30 : '',
                    ref_code :'',
                    gift_code: ''
                  });
                  
                  this.cardName = exp[3];
                  this.PurchasePlanForm.controls['card_number'].disable();
                  this.PurchasePlanForm.controls['card_expiry'].disable();
                  this.PurchasePlanForm.controls['card_cvv'].disable();
                  this.PurchasePlanForm.controls['zip_code'].disable();
                  
                  //this.cookieService.delete('fp_waitlist_previousCard');
                  var temp = exp[0].replace(/ /g, ''); //.replace(' ','');
                  var temp2 = temp.replace(/X/g,'0');
                  this.card_number = (temp2);
                  this.card_expiry  = parseInt(exp[2].replace("/",""));
                  this.zip_code     = exp[1];
              }
              }else{}
          },
          error => {
              console.log("Card Error", error);
        });
        }else{
          if(this.cookieService.get('fp_waitlist_previousCard')){
            var data = this.cookieService.get('fp_waitlist_previousCard');
            console.log(data);
            var exp = data.split(",");
            this.previousCard = exp[0];
            this.hasPreviousCard = true;
            this.PurchasePlanForm.setValue({
              name_on_card:this.cookieService.get('fp_waitlist_fullname'),
              card_number:exp[0],
              card_expiry:exp[2],
              zip_code:exp[1],
              card_cvv :'XXXX',
              term_agree :'',
              fp_authorize :'',
              phone_number:'',
              credit_30 : '',
              ref_code :'',
              gift_code: ''
            });
            
            this.cardName = exp[3];
            this.PurchasePlanForm.controls['card_number'].disable();
            this.PurchasePlanForm.controls['card_expiry'].disable();
            this.PurchasePlanForm.controls['card_cvv'].disable();
            this.PurchasePlanForm.controls['zip_code'].disable();
            
            //this.cookieService.delete('fp_waitlist_previousCard');
            var temp = exp[0].replace(/ /g, ''); //.replace(' ','');
            var temp2 = temp.replace(/X/g,'0');
            this.card_number = (temp2);
            this.card_expiry  = parseInt(exp[2].replace("/",""));
            this.zip_code     = exp[1];
          }
       }
      //  if(this.cookieService.get('fp_waitlist_phone_no')){
      //     this.PurchasePlanForm.setValue({
      //       name_on_card:this.PurchasePlanForm.value.name_on_card,
      //       card_number:'',
      //       card_expiry:'',
      //       zip_code:'',
      //       card_cvv :'',
      //       term_agree :this.PurchasePlanForm.value.term_agree,
      //       fp_authorize :this.PurchasePlanForm.value.fp_authorize,
      //       phone_number:this.cookieService.get('fp_waitlist_phone_no'),
      //       credit_30 :  '',
      //       ref_code : '',
      //       gift_code : ''
      //     });
      //   }
         // var postUrlBanjo = "interactions/getWaitlistTime";  
         var _this = this;
         setTimeout(function(){
          _this.checkIfUserExistBefore(_this.cookieService.get('fp_waitlist_email'))
         },2000);  
         if(window.innerWidth > 767) {
         document.getElementById("card_number").focus();}
  }

editCard(){
      var code = "";
      if(this.PurchasePlanForm.value.ref_code){
        code = this.PurchasePlanForm.value.ref_code;
      }else{
        code = this.referral_code;
      }
      var credit_30 = '';
      if(this.PurchasePlanForm.value.credit_30){
          credit_30 = this.PurchasePlanForm.value.credit_30;
      }

      var gift_code = '';
      if(this.PurchasePlanForm.value.gift_code){
        gift_code = this.PurchasePlanForm.value.gift_code;
      } 

    
        if(this.waitTime == '0'){
          this.PurchasePlanForm.setValue({
            name_on_card:this.PurchasePlanForm.value.name_on_card,
            card_number:'',
            card_expiry:'',
            zip_code:'',
            card_cvv :'',
            term_agree :this.PurchasePlanForm.value.term_agree,
            fp_authorize :'',
            phone_number:this.PurchasePlanForm.value.phone_number,
            credit_30 :  credit_30,
            ref_code : code,
            gift_code : gift_code
          });        }else{
          this.PurchasePlanForm.setValue({
            name_on_card:this.PurchasePlanForm.value.name_on_card,
            card_number:'',
            card_expiry:'',
            zip_code:'',
            card_cvv :'',
            term_agree :this.PurchasePlanForm.value.term_agree,
            fp_authorize :this.PurchasePlanForm.value.fp_authorize,
            phone_number:this.PurchasePlanForm.value.phone_number,
            credit_30 :  credit_30,
            ref_code : code,
            gift_code : gift_code
          });
        }
        //(<HTMLInputElement>document.getElementById("card_number")).value ="";
        this.PurchasePlanForm.controls['card_number'].enable();
        this.PurchasePlanForm.controls['card_expiry'].enable();
        this.PurchasePlanForm.controls['card_cvv'].enable();
        this.PurchasePlanForm.controls['zip_code'].enable();
        this.hasPreviousCard = false;
}
 
showTermService(option: string){
    if(option=="1"){
      let body = document.getElementsByTagName('body')[0];
      body.classList.add("body-position-fixed");
      this.showTermOfService = true;
    }else{
      let body = document.getElementsByTagName('body')[0];
      body.classList.remove("body-position-fixed");
      this.showTermOfService = false;
    }
}
  CharactersOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if(!(charCode >= 65 && charCode <= 122) && (charCode != 32 && charCode != 0)){
      return false;
    }
    return true;
  }
  SetPhoneValidation(event): boolean {
    
    const charCode = (event.which) ? event.which : event.keyCode;
    var first  = event.target.value;
    var second = "";
    if(first.length > 0){
       second = "(";
    }
    first  = first.substr(1,1);
    var last = first.substr(2);
    
    if(first == '1'){
      (<HTMLInputElement>document.getElementById("phone_number")).value = second+last;
      console.log(event.target.value);
      return false;
    }
    //else*/
    //if (charCode > 31 && (charCode < 48 || charCode > 57 || first == '1')   ) {  return false; }
    return true;
  }
  SetCardIcon(cardValue : string ) {
      var first = cardValue.substr(0,1);
      if(first=="3"){
          this.cardName = "American express";
          this.cardImage = "american";
      }
      else if(first=="4"){
        this.cardName = "Visa";
        this.cardImage = "visa";
      }
      else if(first=="5"){
        this.cardName = "Mastercard";
        this.cardImage = "mastercard";
      }
      else if(first=="6"){
        this.cardName = "Discover";
        this.cardImage = "discover";
      }
      else{
        this.cardName = "";
        this.cardImage = "";
      }
  }
  checkZipCode(zip : string ) {
    if(zip.length >3){ 
          let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/plain; application/json; charset=UTF-8');
          
          var API_KEY = "i8Xrw4IWSNJpCKhhqIiJmZ09OSUAQDPsaqkiALoa2Hd1YTc5pX897EAoW3JLbThq";
          //"Ks3OlCOHGyItdbRYU1JqnqcYVjUG8aXn7BhTUagZTu8hNqzQhhLySFffXRdx6f9D";
          
          var API_URL = 'https://www.zipcodeapi.com/rest/'+API_KEY+'/info.json/'+zip+'/degrees';
          var url = btoa(API_URL);
          this.http.post('https://waitlist.thefashionpass.com/zip.php',  { "zip" : url}, { responseType: 'json', headers: headerOptions }).subscribe(
/*          this.http.post(url, { responseType: 'json', headers: headerOptions }).subscribe(*/
            data  => { 
                  //response = JSON.parse(response);
                  const zipJson: any[] = Array.of(data);
                  var response = zipJson[0];

                  if(response.error_code){
                     if(response.error_code ==  401){
                        this.validZipCode = "true";
                        this.city = response.city;
                        this.state = response.state;
                     }else{
                        this.validZipCode = "false";
                     }
                  }else if(response.zip_code){
                      this.validZipCode = "true";
                      this.city = response.city;
                      this.state = response.state;
                  }
                  //this.validZipCode = "true";
            },
            error => {
                console.log("Error", error);
            });
      }
  }
  checkZipCodeValidation(zip) {
    if(zip.length >2){ 
          let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/plain; application/json; charset=UTF-8');
          var API_KEY = "i8Xrw4IWSNJpCKhhqIiJmZ09OSUAQDPsaqkiALoa2Hd1YTc5pX897EAoW3JLbThq";
          
          var API_URL = 'https://www.zipcodeapi.com/rest/'+API_KEY+'/info.json/'+zip+'/degrees';
          var url = btoa(API_URL);
          return this.http.post<string>('https://waitlist.thefashionpass.com/zip.php',  { "zip" : url}, { responseType: 'json', headers: headerOptions });
      }
  }

  NumbersOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {  return false; }
    return true;
  }

  AlphaOnly(event,action): boolean {   
    var str1 = new String( '/^[^`~!@#$%\^&*()+={}|[\]\\:\';"<>?,./]*$/-' ); 
    var index = str1.indexOf( event.key ); 
    if (event.keyCode == 13 && action) {
      if(action == 'ref'){
        this.checkRefCode(event.target.value)
      }else{
        this.checkGiftCode(event.target.value)
      }
      return false;
    }
    if(index >= 0){
      return false;
    }
   
    return true;
  }
  clearPhoneError(){
    this.phone_number_error = "false";
  }

  countdown_restart(){
    this.counterLeftTime = 300;
    setTimeout(() => this.counter.restart()); 
  }

  submitPurchase(){
    console.log(this.zip_code);
    if (this.PurchasePlanForm.valid) {
      this.spinner.show();
      this.data.verifyVipWaitlist().subscribe(
        checkVipResponse=>{
          console.log(checkVipResponse);
          if(checkVipResponse.toString() == "true"){
            var refChecking = '';
            var rc = '';
            if(this.referral_code) {
              rc = this.referral_code;
              if(rc != ''){
                refChecking = "true";
              } 
            }
            var giftChecking = '';
            var gc = '';
            if(this.validGiftCode) {
              gc = this.validGiftCode;
              if(gc != ''){
                giftChecking = "true";
              } 
            }
            var promoChecking = '';
            var pc = '';
            if(this.validPromoCode) {
              pc = this.validPromoCode;
              if(pc != ''){
                promoChecking = "true";
              } 
            }
            
            if(this.PurchasePlanForm.value.zip_code){
                this.zip_code = this.PurchasePlanForm.value.zip_code;
            }else{
                this.zip_code;
            }
            this.checkZipCodeValidation(this.zip_code).subscribe(
              data  => {
                    const zipJson: any[] = Array.of(data);
                    var response = zipJson[0];
      
                    if(response.error_code){
                       if(response.error_code ==  401){
                          this.validZipCode = "true";
                          this.city = response.city;
                          this.state = response.state;
                       }else{
                          this.validZipCode = "false";
                          this.spinner.hide();
                       }
                    }else if(response.zip_code){
                        this.validZipCode = "true";
                        this.city = response.city;
                        this.state = response.state;
                    }
                    console.log(this.validZipCode);
      
                    if((this.validRefCode == refChecking || this.validPromoCode == promoChecking) && this.validZipCode == 'true' && this.validGiftCode == giftChecking){  
                      setTimeout(() => this.counter.restart()); 
              
                      this.name_on_card = this.PurchasePlanForm.value.name_on_card.trim(); 
                      this.phone_number = this.PurchasePlanForm.value.phone_number; 

                      if(this.cookieService.get('fp_waitlist_vip_customer')){
                        if(this.PurchasePlanForm.value.card_number){
      
                          this.card_number  = this.PurchasePlanForm.value.card_number;
                          this.card_expiry  = this.PurchasePlanForm.value.card_expiry;
                          this.card_cvv     = this.PurchasePlanForm.value.card_cvv;
                          this.zip_code     = this.PurchasePlanForm.value.zip_code;
                        
                          var wt_expirymonthyeararr =  this.card_expiry.toString();
                          var wt_expiry_month = wt_expirymonthyeararr.substr(0, 2);
                          var wt_expiry_year = wt_expirymonthyeararr.substr(2, 2);

                          this.data.checkCustomerCard(this.card_number, this.card_expiry, this.zip_code).subscribe(
                            data=>{
                                  // console.log("Card check Response =>"+ data);

                                    var resCardCheck = ''; //(data.toString());

                                    if(resCardCheck != "block"){
                                        //this.cookieService.set('fp_waitlist_attempt_card', resCardCheck.toString());
                                        this.res =  this.data.purchasePlanM2(this.name_on_card, this.card_number, this.card_cvv, this.zip_code, wt_expiry_month, wt_expiry_year, this.currentPlanePrice);
                                        this.res.subscribe(
                                            data => {
                                            var stripeResponse = (data);
                                            if(stripeResponse.error){
                                              if(stripeResponse.error.decline_code == "fraudulent"){
                                                this.cardError = "true";
                                                //this.CardErrorMessage = "Your card was declined because it is linked to a history of fraudulent activity. This may be because you have reported fraudulent activity on your card in the past. Please choose a different card.";
                                                this.CardErrorMessage = "*This card could not be authorized. Please provide another payment method.";
                                                this.spinner.hide();
                                              }
                                              else{
                                                this.cardError = "true";
                                                this.CardErrorMessage = "*"+stripeResponse.error.message;
                                                // this.CardErrorMessage = "*This card could not be authorized. Please provide another payment method.";
                                                this.spinner.hide();
                                              }
                                            }else{
                                            // this.cookieService.set( 'CustomerStripeID',  btoa(stripeResponse.id));
                                              // Make Card default
                                                let settings = JSON.stringify({
                                                    "url": "/customers/" + atob(this.cookieService.get("CustomerStripeID")),
                                                    "method": "POST",
                                                    "headers": {
                                                        "authorization": "Bearer "
                                                    },
                                                    "data": {
                                                        "default_source": stripeResponse.id,
                                                    }
                                                });
                                                this.data.UpdateWaitlistCustomer(settings,this.currentPlanePrice,this.card_number, this.cardName, this.referral_code, this.zip_code, this.city, this.state, this.phone_number, this.card_expiry)
                                                .subscribe(
                                                  data=>{
                                                    // if(data=="1"){
                                                    //     this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                                    //     this.router.navigate(['priority-list']);
                                                    // }
                                                    this.waitlistCusRes = data;
                                                    if(this.waitlistCusRes.status == "1"){
                                                      if(this.cookieService.get('fp_waitlist_email_type')){
                                                        var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":this.cookieService.get('fp_waitlist_email_type'),"event":'completed_checkout'};
                                                        console.log(obj);
                                                        this.data.setUserActivityBanjo(obj).subscribe(data => {
                                                          console.log('data responce'+data)
                                                        });
                                                      }
                                                      new gtag_report_conversion(this.router.url,this.cookieService.get('fp_waitlist_email'));
                                                      console.log("--------------echo wait time:"+ this.waitTime);
                                                      if(this.waitTime == '0'){
                                                        console.log('---------------- success return 1')
                                                        var requestCount = 0;
                                                                this.requestPending = 'false';
                                                                var _this = this;
                                                                var userChekInterval = setInterval(function(){ 
                                                                  if(_this.requestPending == 'false'){
                                                                    requestCount++
                                                                    _this.checkIfUserExist(_this.cookieService.get('fp_waitlist_email'))
                                                                    console.log(requestCount+' ----- check user request count')
                                                                    if(_this.checkUserCreate == 1 || requestCount > 9){
                                                                      clearInterval(userChekInterval);
                                                                      if(requestCount > 9){
                                                                        var emailObj = {"email":_this.cookieService.get('fp_waitlist_email'),"subject":'User plan name not inserted on waitlist checkout'};
                                                                        _this.sentEmail(emailObj);
                                                                      }
                                                                      _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                                    } 
                                                                    if(_this.checkUserCreate == 2){
                                                                      clearInterval(userChekInterval);
                                                                      _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                                    }
                                                                  } 
                                                                  
                                                                }, 1000);
                                                        // if(this.checkUserCreate == 1){
                                                        //   this.gotoReward()
                                                        // }else{
                                                        //   this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                                        //   this.gotoReward()
                                                        // }
                                                      }else{
                                                        this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                                        this.router.navigate(['priority-list']);
                                                      }
                                                       
                                                    }else if (this.waitlistCusRes.status == "2"){
                                                      this.spinner.hide();
                                                      this.validPromoCode = 'false';
                                                      var ref_code_check = 0;
                                                      if( this.validRefCode == "true"){
                                                          ref_code_check = this.refDiscount;
                                                      }
                                                      this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,'0');
                                                      this.promoCredit = 0;
                                                      this.PurchasePlanForm.controls['ref_code'].enable(); 
                                                      this.cookieService.delete('fp_waitlist_promoCode');
                                                      this.check30Credit = '0';
                                                      this.PurchasePlanForm.controls['ref_code'].setValue(''); 
                                                      console.log('This coupon is only for new customer');
                                                      this.validPromoCodeMessage = 'This code is only valid for new members.'
                                                    }else if (this.waitlistCusRes.status == "0"){
                                                      this.spinner.hide();
                                                      this.cardError = "true";
                                                      this.CardErrorMessage = this.waitlistCusRes.message;
                                                      this.cookieService.delete('fp_waitlist_previousCard');
                                                      this.cardImage = '';
                                                      this.editCard();
                                                    }
                                                });
                                              
                                            }
                                            },
                                            error => {
                                              console.log("Error: Card not added in stripe account!");
                                              console.log("Error", error);
                                          }
                                        );
                                    }
                            },
                            error=>{
                                  console.log("Card check Error=>",error);
                            }
                          );
                      }else{
                          this.data.UpdateWaitlistCustomer("",this.currentPlanePrice,this.card_number, this.cardName, this.referral_code, this.zip_code, this.city, this.state, this.phone_number, this.card_expiry)
                          .subscribe(
                            data=>{
                              // if(data=="1"){
                              //     this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                              //     this.router.navigate(['priority-list']);
                              // }
                              this.waitlistCusRes = data;
                              if(this.waitlistCusRes.status == "1"){
                                if(this.cookieService.get('fp_waitlist_email_type')){
                                  var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":this.cookieService.get('fp_waitlist_email_type'),"event":'completed_checkout'};
                                  console.log(obj);
                                  this.data.setUserActivityBanjo(obj).subscribe(data => {
                                    console.log('data responce'+data)
                                  });
                                }
                                new gtag_report_conversion(this.router.url,this.cookieService.get('fp_waitlist_email'));
                                console.log("--------------echo wait time:"+ this.waitTime);
                                if(this.waitTime == '0'){
                                  console.log('---------------- success return 1')
                                  var requestCount = 0;
                                  this.requestPending = 'false';
                                  var _this = this;
                                  var userChekInterval = setInterval(function(){ 
                                    if(_this.requestPending == 'false'){
                                      requestCount++
                                      _this.checkIfUserExist(_this.cookieService.get('fp_waitlist_email'))
                                      console.log(requestCount+' ----- check user request count')
                                      if(_this.checkUserCreate == 1 || requestCount > 9){
                                        clearInterval(userChekInterval);
                                        if(requestCount > 9){
                                          var emailObj = {"email":_this.cookieService.get('fp_waitlist_email'),"subject":'User plan name not inserted on waitlist checkout'};
                                          _this.sentEmail(emailObj);
                                        }
                                        _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                      }
                                      if(_this.checkUserCreate == 2){
                                        clearInterval(userChekInterval);
                                        _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                      }
                                    } 
                                    
                                  }, 1000);
                                  // this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                  // if(this.checkUserCreate == 1){
                                  //   this.gotoReward()
                                  // }else{
                                  //   this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                  //   this.gotoReward()
                                  // }
                                }else{
                                  this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                  this.router.navigate(['priority-list']);
                                }
                                 
                              }else if (this.waitlistCusRes.status == "2"){
                                this.spinner.hide();
                                this.validPromoCode = 'false';
                                var ref_code_check = 0;
                                if( this.validRefCode == "true"){
                                    ref_code_check = this.refDiscount;
                                }
                                this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,'0');
                                this.promoCredit = 0;
                                this.PurchasePlanForm.controls['ref_code'].enable(); 
                                this.cookieService.delete('fp_waitlist_promoCode');
                                console.log('This coupon is only for new customer');
                                this.check30Credit = '0';
                                this.PurchasePlanForm.controls['ref_code'].setValue(''); 
                                this.validPromoCodeMessage = 'This code is only valid for new members.'
                              }else if (this.waitlistCusRes.status == "0"){
                                this.spinner.hide();
                                this.cardError = "true";
                                this.CardErrorMessage = this.waitlistCusRes.message;
                                this.cookieService.delete('fp_waitlist_previousCard');
                                this.cardImage = '';
                                this.editCard();
                              }
                          });
                      }

                      }else{
                        this.data.setPhoneNumber(this.phone_number).subscribe(
                          PhoneResponse =>{ 
                            if(PhoneResponse.toString() == "true" || this.cookieService.get('fp_waitlist_vip_customer') ){
                                if(this.PurchasePlanForm.value.card_number){
        
                                    this.card_number  = this.PurchasePlanForm.value.card_number;
                                    this.card_expiry  = this.PurchasePlanForm.value.card_expiry;
                                    this.card_cvv     = this.PurchasePlanForm.value.card_cvv;
                                    this.zip_code     = this.PurchasePlanForm.value.zip_code;
                                  
                                    var wt_expirymonthyeararr =  this.card_expiry.toString();
                                    var wt_expiry_month = wt_expirymonthyeararr.substr(0, 2);
                                    var wt_expiry_year = wt_expirymonthyeararr.substr(2, 2);
        
                                    this.data.checkCustomerCard(this.card_number, this.card_expiry, this.zip_code).subscribe(
                                      data=>{
                                              var resCardCheck = ""; //(data.toString());
                                              if(resCardCheck != "block"){
                                                //this.cookieService.set('fp_waitlist_attempt_card', resCardCheck.toString());
                                                this.res =  this.data.purchasePlanM2(this.name_on_card, this.card_number, this.card_cvv, this.zip_code, wt_expiry_month, wt_expiry_year, this.currentPlanePrice);
                                                this.res.subscribe(
                                                  data => {
                                                    var stripeResponse = (data);
                                                    if(stripeResponse.error){
                                                      if(stripeResponse.error.decline_code == "fraudulent"){
                                                        this.cardError = "true";
                                                        //this.CardErrorMessage = "Your card was declined because it is linked to a history of fraudulent activity. This may be because you have reported fraudulent activity on your card in the past. Please choose a different card.";
                                                        this.CardErrorMessage = "*This card could not be authorized. Please provide another payment method.";
                                                        this.spinner.hide();
                                                      }
                                                      else{
                                                        this.cardError = "true";
                                                        this.CardErrorMessage = "*"+stripeResponse.error.message;
                                                        // this.CardErrorMessage = "*This card could not be authorized. Please provide another payment method.";
                                                        this.spinner.hide();
                                                      }
                                                    }else{
                                                    // this.cookieService.set( 'CustomerStripeID',  btoa(stripeResponse.id));
                                                      // Make Card default
                                                        let settings = JSON.stringify({
                                                            "url": "/customers/" + atob(this.cookieService.get("CustomerStripeID")),
                                                            "method": "POST",
                                                            "headers": {
                                                                "authorization": "Bearer "
                                                            },
                                                            "data": {
                                                                "default_source": stripeResponse.id,
                                                            }
                                                        });
                                                        this.data.UpdateWaitlistCustomer(settings,this.currentPlanePrice,this.card_number, this.cardName, this.referral_code, this.zip_code, this.city, this.state, this.phone_number, this.card_expiry)
                                                        .subscribe(
                                                          data=>{
                                                          // console.log("data:", data)
                                                            // if(data=="1"){
                                                            //     this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                                            //     this.router.navigate(['priority-list']);
                                                            // }
                                                            this.waitlistCusRes = data;
                                                            if(this.waitlistCusRes.status == "1"){
                                                              if(this.cookieService.get('fp_waitlist_email_type')){
                                                                var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":this.cookieService.get('fp_waitlist_email_type'),"event":'completed_checkout'};
                                                                console.log(obj);
                                                                this.data.setUserActivityBanjo(obj).subscribe(data => {
                                                                  console.log('data responce'+data)
                                                                });
                                                              }
                                                              new gtag_report_conversion(this.router.url,this.cookieService.get('fp_waitlist_email'));
                                                              console.log("--------------echo wait time:"+ this.waitTime);
                                                              if(this.waitTime == '0'){
                                                                console.log('---------------- success return 2')
                                                                var requestCount = 0;
                                                                this.requestPending = 'false';
                                                                var _this = this;
                                                                var userChekInterval = setInterval(function(){ 
                                                                  if(_this.requestPending == 'false'){
                                                                    requestCount++
                                                                    _this.checkIfUserExist(_this.cookieService.get('fp_waitlist_email'))
                                                                    console.log(requestCount+' ----- check user request count')
                                                                    if(_this.checkUserCreate == 1 || requestCount > 9){
                                                                      clearInterval(userChekInterval);
                                                                      if(requestCount > 9){
                                                                        var emailObj = {"email":_this.cookieService.get('fp_waitlist_email'),"subject":'User plan name not inserted on waitlist checkout'};
                                                                        _this.sentEmail(emailObj);
                                                                      }
                                                                      _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                                    }
                                                                    if(_this.checkUserCreate == 2){
                                                                      clearInterval(userChekInterval);
                                                                      _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                                    }
                                                                  } 
                                                                  
                                                                }, 1000);
                                                                // this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                                                // if(this.checkUserCreate == 1){
                                                                //   this.gotoReward()
                                                                // }else{
                                                                //   this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                                                //   this.gotoReward()
                                                                // }
                                                              }else{
                                                                this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                                                this.router.navigate(['priority-list']);
                                                              }
                                                               
                                                            }else if (this.waitlistCusRes.status == "2"){
                                                              this.spinner.hide();
                                                              this.validPromoCode = 'false';
                                                              var ref_code_check = 0;
                                                              if( this.validRefCode == "true"){
                                                                  ref_code_check = this.refDiscount;
                                                              }
                                                              this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,'0');
                                                              this.promoCredit = 0;
                                                              this.PurchasePlanForm.controls['ref_code'].enable(); 
                                                              this.cookieService.delete('fp_waitlist_promoCode');
                                                              console.log('This coupon is only for new customer');
                                                              this.check30Credit = '0';
                                                              this.PurchasePlanForm.controls['ref_code'].setValue(''); 
                                                              this.validPromoCodeMessage = 'This code is only valid for new members.'
                                                            }else if (this.waitlistCusRes.status == "0"){
                                                              this.spinner.hide();
                                                              this.cardError = "true";
                                                              this.CardErrorMessage = this.waitlistCusRes.message;
                                                              this.cookieService.delete('fp_waitlist_previousCard');
                                                              this.cardImage = '';
                                                              this.editCard();
                                                            }
                                                        });
                                                      
                                                    }
                                                },
                                                error => {
                                                    console.log("Error: Card not added in stripe account!");
                                                    console.log("Error", error);
                                                }
                                                );
                                              }
                                      },
                                      error=>{
                                            console.log("Card check Error=>",error);
                                      }
                                    );
                                }else{
                                    this.data.UpdateWaitlistCustomer("",this.currentPlanePrice,this.card_number, this.cardName, this.referral_code, this.zip_code, this.city, this.state, this.phone_number, this.card_expiry)
                                    .subscribe(
                                      data=>{
                                        // if(data=="1"){
                                        //     this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                        //     this.router.navigate(['priority-list']);
                                        // }
                                        this.waitlistCusRes = data;
                                        if(this.waitlistCusRes.status == "1"){
                                          if(this.cookieService.get('fp_waitlist_email_type')){
                                            var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":this.cookieService.get('fp_waitlist_email_type'),"event":'completed_checkout'};
                                            console.log(obj);
                                            this.data.setUserActivityBanjo(obj).subscribe(data => {
                                              console.log('data responce'+data)
                                            });
                                          }
                                          new gtag_report_conversion(this.router.url,this.cookieService.get('fp_waitlist_email'));
                                          console.log("--------------echo wait time:"+ this.waitTime);
                                          if(this.waitTime == '0'){
                                            console.log('---------------- success return 3')
                                            var requestCount = 0;
                                            this.requestPending = 'false';
                                            var _this = this;
                                            var userChekInterval = setInterval(function(){ 
                                              if(_this.requestPending == 'false'){
                                                requestCount++
                                                _this.checkIfUserExist(_this.cookieService.get('fp_waitlist_email'))
                                                console.log(requestCount+' ----- check user request count')
                                                if(_this.checkUserCreate == 1 || requestCount > 9){
                                                  clearInterval(userChekInterval);
                                                  if(requestCount > 9){
                                                    var emailObj = {"email":_this.cookieService.get('fp_waitlist_email'),"subject":'User plan name not inserted on waitlist checkout'};
                                                    _this.sentEmail(emailObj);
                                                  }
                                                  _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                }
                                                if(_this.checkUserCreate == 2){
                                                  clearInterval(userChekInterval);
                                                  _this.gotoReward('1',_this.waitlistCusRes.subscription_id)
                                                }
                                              } 
                                              
                                            }, 1000);
                                            // this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                            // if(this.checkUserCreate == 1){
                                            //   this.gotoReward()
                                            // }else{
                                            //   this.checkUserCreate = this.checkIfUserExist(this.cookieService.get('fp_waitlist_email'))
                                            //   this.gotoReward()
                                            // }
                                          }else{
                                            this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
                                            this.router.navigate(['priority-list']);
                                          }
                                           
                                        }else if (this.waitlistCusRes.status == "2"){
                                          this.spinner.hide();
                                          this.validPromoCode = 'false';
                                          var ref_code_check = 0;
                                          if( this.validRefCode == "true"){
                                              ref_code_check = this.refDiscount;
                                          }
                                          this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,'0');
                                          this.promoCredit = 0;
                                          this.PurchasePlanForm.controls['ref_code'].enable(); 
                                          this.cookieService.delete('fp_waitlist_promoCode');
                                          console.log('This coupon is only for new customer');
                                          this.check30Credit = '0';
                                          this.PurchasePlanForm.controls['ref_code'].setValue(''); 
                                          this.validPromoCodeMessage = 'This code is only valid for new members.'
                                        }else if (this.waitlistCusRes.status == "0"){
                                          this.spinner.hide();
                                          this.cardError = "true";
                                          this.CardErrorMessage = this.waitlistCusRes.message;
                                          this.cookieService.delete('fp_waitlist_previousCard');
                                          this.cardImage = '';
                                          this.editCard();
                                        }
                                    });
                                }
        
                            }else if(PhoneResponse.toString() == "false") {
                                  this.showValidationError = "true";
                                  this.phone_number_error = "Phone number is invalid."; 
                                  this.spinner.hide();
                            }else{
                                  this.cookieService.set('fp_waitlist_attempt_phone',this.phone_number);
                                  this.showValidationError = "true";
                                  this.phone_number_error = "Phone number "+PhoneResponse.toString()+"."; 
                                  this.spinner.hide();
                            }
                          /***************** */
                        },
                        error =>{
                          console.log("Card check Error=>",error);
                        }
                      );
                    }
                    }else{
                      this.spinner.hide();
                    }  
              });
             
          }else{
            this.cookieService.set( 'fp_waitlist_step',  "priority-list" );
            this.router.navigate(['priority-list']);
          }

        }
      );

      
    }else{
          this.showValidationError = "true";
    }
  }
  clearCardError(){
    this.validZipCode =  "";
  }
  clearZipValidation(){
    this.CardErrorMessage = "";
  }
  gotoPlans(){
    this.cookieService.set( 'fp_waitlist_step',  "plans" );
    this.router.navigate(['plans']);
  }
  checkRefCode(refCode: string){
      this.PurchasePlanForm.controls['fp_authorize'].enable();
      this.PurchasePlanForm.controls['credit_30'].enable();
      this.ApplyFreeCoupon = false;
      this.promoRefButton = false;
      if(refCode.length >2){
            var setReferralCodeFunction = false; 
            this.PurchasePlanForm.controls['ref_code'].disable();
            
            
            if(this.cookieService.get( 'fp_waitlist_referrals')){
               var ref_arry = this.cookieService.get( 'fp_waitlist_referrals');
               var ref_arry2 = ref_arry.split(", ");
               if(ref_arry2.indexOf(refCode) == -1){
                      setReferralCodeFunction = true;
              }else{
                      setReferralCodeFunction = false;
                      this.showRefError = "true";
                      this.validRefCode = "false";
                      this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,'0');
                      this.PurchasePlanForm.controls['ref_code'].enable();
               }
            }else{
                      setReferralCodeFunction = true;
            }

       if(setReferralCodeFunction == true){
         this.validPromoCode = '';
         this.check30Credit = '0';
         this.validPromoCodeMessage = '';
          this.data.checkFriendReferralCode(refCode).subscribe(
            data=>{
              this.promoCodeData = data;
                  if(this.promoCodeData.status == "1"){
                    this.promoCredit = '0';
                      this.validPromoCode = "false";
                      this.validRefCode = "true";
                      this.showRefError = "false";
                      this.promoCodeAllowFor = 'new';
                      this.calculateCard(this.PriorityDiscount, this.refDiscount, this.acountCredit,this.promoCredit);
                      this.referral_code = refCode.toUpperCase();
                      this.PurchasePlanForm.controls['ref_code'].disable();
                      this.ApplyFreeCoupon = false;
                      this.promoCredit = '0';
                      this.PurchasePlanForm.controls['credit_30'].enable();
                      this.check30Credit = '1';
                      this.dynemicCreditForCoupon = parseInt(this.promoCodeData.amount);
                      this.cookieService.delete('fp_waitlist_promoCode')
                  }
                  else if(this.promoCodeData.status == "vip"){
                    this.validRefCode = "true";
                    this.showRefError = "false";
                    this.calculateCard(this.PriorityDiscount, this.refDiscount, this.acountCredit,'0');
                    this.referral_code = refCode.toUpperCase();
                    this.PurchasePlanForm.controls['ref_code'].disable();
                    this.ApplyFreeCoupon = true;
                    this.cookieService.set('fp_waitlist_vip_customer', 'vip');
                    this.PurchasePlanForm.controls['fp_authorize'].disable();
                    this.PurchasePlanForm.controls['credit_30'].disable();
                    this.PurchasePlanForm.get('phone_number').setValidators([firstLetterCheck]);
                  }else if (this.promoCodeData.status == 'promo'){
                    console.log('promo')
                    var reponceData = JSON.parse(this.promoCodeData.response);
                    console.log(reponceData)
                    var amount = reponceData.data.amount_off;
                    var calcAmount ;
                    this.cookieService.set('fp_waitlist_promoCode', refCode);
                    this.couponAmount_type = 'amount';
                    if(this.currentPlaneName == "socialite"){
                      this.currentPlanePrice = this.data.SocialitePlanPrice;
                    }
                    else if(this.currentPlaneName == "trendsetter"){
                        this.currentPlanePrice = (this.data.TrendsetterPlanPrice);
                    }
                    else if(this.currentPlaneName == "wanderlust"){
                      this.currentPlanePrice = (this.data.WanderlustPlanPrice);
                    }
                    if(amount == null){
                      amount = reponceData.data.percent_off;
                      this.couponAmount_type = 'persontage';
                      var currentPlanePrice = parseInt(this.currentPlanePrice);
                      calcAmount = (currentPlanePrice * amount / 100);
                      this.dynemicCreditForCoupon = calcAmount;
                    }else{
                      calcAmount = amount/100;
                      amount = calcAmount;
                      this.dynemicCreditForCoupon = amount;
                    }
                    var ref_code_check = 0;
                    if( this.validRefCode == "true"){
                        ref_code_check = this.refDiscount;
                    }
                    this.promoCodeAllowFor = reponceData.data.metadata.allowfor
                    this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,calcAmount); 
                    this.promoCreditDiscount = amount;
                    this.promoCredit = calcAmount; 
                    this.validPromoCode = "true";
                    this.showRefError = "false";
                    this.validRefCode = "false";
                    this.PurchasePlanForm.controls['ref_code'].enable();
                    this.check30Credit = '1';
                    // this.PurchasePlanForm.controls['credit_30'].enable();
                    this.referral_code = '';
                  }else if (this.promoCodeData.status == 'gift'){
                    this.showGiftCode = 'true';
                    this.PurchasePlanForm.controls['gift_code'].setValue(this.promoCodeData.coupon);
                    this.checkGiftCode(this.promoCodeData.coupon);
                    this.PurchasePlanForm.controls['ref_code'].setValue('');
                    this.PurchasePlanForm.controls['ref_code'].enable();
                    this.promoRefButton = true;
                  }
                  else{
                      this.cookieService.delete('fp_waitlist_promoCode');
                      // this.cookieService.delete('creditForMembers');
                      this.promoRefButton = true;
                      this.promoCredit = '0';
                      this.validPromoCode = "false";
                      this.validPromoCodeMessage = this.promoCodeData.message;
                      this.showRefError = "true";
                      this.validRefCode = "false";
                      this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,this.promoCredit);
                      this.PurchasePlanForm.controls['ref_code'].enable();
                      this.dynemicCreditForCoupon = 0;
                      // this.PurchasePlanForm.controls['credit_30'].disable();
                      
                    }
            });
       }
           
      }else{
        this.promoRefButton = true;
        this.cookieService.delete('fp_waitlist_promoCode');
        this.check30Credit = '0';
        this.promoCredit = '0';
        // this.PurchasePlanForm.controls['credit_30'].disable();
        this.validRefCode = ""; 
        this.validPromoCode = "false";
        this.validPromoCodeMessage = "";
        this.dynemicCreditForCoupon = 0;
        this.calculateCard(this.PriorityDiscount, 0, this.acountCredit,'0');
                    
      } 
  }

  checkGiftCode(giftCode: string){
    if(this.checkGiftReq == true){
      return false;
    }
    this.giftButton = false;
    this.checkGiftReq = true;
    this.ApplyFreeCoupon = false;
    if(giftCode.length >2){
          this.data.checkGiftCode(giftCode).subscribe(
            data=>{
                  console.log(data);
                  this.giftCodeData = data;
                  if(this.giftCodeData.status == "0"){ 
                    this.giftButton = true;
                    this.invalidGiftCodeMessage = this.giftCodeData.message;
                    this.validGiftCode = "false";
                    this.PurchasePlanForm.controls['gift_code'].enable();
                  } 
                  else{
                    //data.gift_value
                    this.validGiftCode = "true";
                    const responseJson: any[] = Array.of(data);
                    var response = responseJson[0];
                    var coupon_id = response.coupon_id;
                    var amount = response.gift_value;

                    this.PurchasePlanForm.controls['gift_code'].disable();
                    
                    var stripe_id = atob(this.cookieService.get( 'CustomerStripeID'));

                    this.data.ApplyGiftCode(stripe_id, coupon_id, amount).subscribe(
                      data=>{
                          console.log(data);
                          this.data.getWaitlistCredit(this.cookieService.get('fp_waitlist_email')).subscribe(
                            data  => {  
                                  const usersJson: any[] = Array.of(data);
                                  var response = usersJson[0];
                                  if(response[2] % 1 != 0){
                                      if(response[2]){
                                        this.acountCredit = parseFloat(response[2]); 
                                      }
                                      if(response[1]){
                                        this.refDiscount =  parseFloat(response[1]); 
                                      }
                                      if(response[0]){
                                        this.PriorityDiscount = parseFloat(response[0]);
                                      } 
                                  }else{
                                      if(response[2]){
                                        this.acountCredit = parseInt(response[2]); 
                                      }
                                      if(response[1]){
                                        this.refDiscount =  parseInt(response[1]); 
                                      }
                                      if(response[0]){
                                        this.PriorityDiscount = parseInt(response[0]);
                                      } 
                      
                                  }
                                  var ref_code_check = 0;
                                  if( this.validRefCode == "true"){
                                      ref_code_check = this.refDiscount;
                                  }
                                  this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,this.promoCredit);  
                                  this.showCheckoutBox = true;
                            },
                            error => {
                                console.log("Error", error);
                            }); 
                          
                      });
                  }
                  this.checkGiftReq = false;
            });

    }else{
              this.validGiftCode = "";
              this.giftButton = true;
              var ref_code_check = 0;
              if( this.validRefCode == "true"){
                  ref_code_check = this.refDiscount;
              }
              this.checkGiftReq = false;
              this.calculateCard(this.PriorityDiscount, ref_code_check, this.acountCredit,this.promoCredit );
    } 
  }

  showFinishMessage(){
    this.counterLeftTime = 300;
    setTimeout(() => this.counter.restart());
  }
  enableReferalField(){
    console.log("Ref Click"+this.validRefCode);
    if(this.validRefCode == "true"){
      this.validRefCode = "";
      this.referral_code = '';
      this.PurchasePlanForm.controls['ref_code'].enable();
      this.refCodeInput.nativeElement.focus();
    }
    this.promoRefButton = true;
  }

  SetCardExpiry(ce : string){
  // console.log(ce);
    if(ce.length > 0){
      var firstLetter =  parseInt(ce.substr(0,1));
      var lastLetters =  ce.substr(1, ce.length-2);
      lastLetters = lastLetters.replace("/","");
      if(firstLetter > 1){
          console.log(firstLetter);
          (<HTMLInputElement>document.getElementById("card_expiry_val")).value = "0"+firstLetter+"/"+lastLetters;
      }
    }
  }

  calculateCard(Discount, Referral, Credit, PromoAmount){
    if(PromoAmount == undefined){
      PromoAmount = 0;
    }
    this.currentPlaneName = this.cookieService.get('fp_waitlist_selected_plan')
    this.customer_fullname = this.cookieService.get("fp_waitlist_fullname");
    var oldCustomer = "false";
    if(this.cookieService.get("fp_waitlist_last_order") || this.creditForMembers == 0){
      console.log('test credit ----------')
        this.PriorityDiscount = 0;
        Discount = 0;
        this.PurchasePlanForm.controls['credit_30'].disable();
        oldCustomer = "true";
        if((Referral != '0' || PromoAmount != '0') && this.promoCodeAllowFor.indexOf('n') >  -1){
          this.PurchasePlanForm.controls['credit_30'].enable();
        }
    }else{
      if(Referral == '0' && PromoAmount == '0'){
        // this.PurchasePlanForm.controls['credit_30'].disable();
      }
      
    }
    if(this.creditForMembers > 0 && Discount == 0){
      PromoAmount = PromoAmount+this.creditForMembers;
      // this.PurchasePlanForm.controls['credit_30'].disable();
    }
    if(this.waitTime == '0'){
      this.PurchasePlanForm.controls['fp_authorize'].disable();
    }
    if(this.currentPlaneName =="socialite"){
      this.currentPlanePrice       = this.data.SocialitePlanPrice;
      this.currentPlaneDescription = this.data.SocialitePlanDescription;
        if( oldCustomer == "true" && this.ApplyBlackFriday == true ){
          Discount = 20;
          this.PriorityDiscount = 20;
        }
      }
    else if(this.currentPlaneName =="trendsetter"){
          this.currentPlanePrice       = (this.data.TrendsetterPlanPrice);
          this.currentPlaneDescription = this.data.TrendsetterPlanDescription;
          if( oldCustomer == "true" && this.ApplyBlackFriday == true){
            Discount = 30;
            this.PriorityDiscount = 30;
          }
      }
    else if(this.currentPlaneName =="wanderlust"){
          this.currentPlanePrice       = (this.data.WanderlustPlanPrice);
          this.currentPlaneDescription = this.data.WanderlustPlanDescription;
          if( oldCustomer == "true" && this.ApplyBlackFriday == true){
            Discount = 40;
            this.PriorityDiscount = 40;
          }
      }

      if( oldCustomer == "false" && this.ApplyBlackFriday == true){
        Discount = parseFloat(this.currentPlanePrice) * 0.5;
        this.PriorityDiscount = Discount;
        this.PurchasePlanForm.controls['credit_30'].disable();
      }

      if(Credit % 1 != 0){
          this.FirstMonthPrice  = parseFloat(this.currentPlanePrice)-parseFloat(Discount)-parseFloat(Referral)-parseFloat(Credit)-parseFloat(PromoAmount);
          this.FirstMonthPrice  = this.FirstMonthPrice.toFixed(2)
      }else{
        this.FirstMonthPrice   = parseFloat(this.currentPlanePrice)-parseFloat(Discount)-parseFloat(Referral)-parseFloat(Credit)-parseFloat(PromoAmount);
        if(this.FirstMonthPrice % 1 != 0){
          this.FirstMonthPrice  = this.FirstMonthPrice.toFixed(2);
        }
        
      }

    //this.FirstMonthPrice         = CurPrice; //-parseInt(Referral)- parseInt(Discount)- parseInt(Credit);
    if(this.FirstMonthPrice < 1){
      this.FirstMonthPrice = 0;
    }

  }
  setOutline(position){
    //console.log('positin: '+position);
      if(position==1){
        this.showBorder1 = true;
      }else{
        this.showBorder2 = true;
      }
  }
  HideOutline(position){
        if(position==1){
          this.showBorder1 = false;
        }else{
          this.showBorder2 = false;
        }
  }
  checkIfUserExistBefore(email){
    this.data.getShopifyCustomer(email).subscribe(
      data => {
        this.customerDetail = data;
        var newData =   JSON.parse(this.customerDetail);
        if(newData){
          if(newData.customers.length > 0 && newData.customers[0].tags.indexOf('plan_name') > -1 && newData.customers[0].tags.indexOf('Paused') == -1){
            console.log(newData.customers[0].tags);
            this.gotoReward('0','');
          }
        }
        
     
    })
  }
  checkIfUserExist(email){
    this.requestPending = 'true';
    this.data.getShopifyCustomer(email).subscribe(
      data => {
        this.customerDetail = data;
        var newData =   JSON.parse(this.customerDetail);
        this.requestPending = 'false';
        if(newData){
          console.log(newData.customers[0].tags);
          if(newData.customers[0].tags.indexOf('plan_name') > -1){
            this.checkUserCreate = 1;
          }else{
            this.checkUserCreate = 0;
          }
          if(newData.customers[0].tags.indexOf('Waitlistchargefailed') > -1){
            this.checkUserCreate = 2;
          }
        }
        
     
    })
  }
  gotoReward(subcriber,subscription_id){
    var checksub = '';
    if(subcriber == '1'){
      checksub = "&zeroHoursubcriber"+subcriber;
    }
    var userType = '';
    if(this.cookieService.get('fp_waitlist_loggedin_customer') && this.cookieService.get('fp_waitlist_last_order')){
      userType = "&userType=returningLogedin";
    }else if(this.cookieService.get('fp_waitlist_last_order')){
      userType = "&userType=returning";
    }else{
      userType = "&userType=newCustomer";
    }
    var email   = this.cookieService.get('fp_waitlist_email');
    var pass    = atob(this.cookieService.get('fp_waitlist_tempvalue'));
    var selectedPlan = this.cookieService.get('fp_waitlist_selected_plan');
    if(this.cookieService.get('fp_waitlist_tempvalue')){
      var fname = btoa(this.cookieService.get('fp_waitlist_first_name'));
      var encoded = "?login="+btoa(email+"(##)"+pass)+"&fname="+fname+checksub+userType+"&signedup=true&selectedPlan="+selectedPlan+"&total_amount="+this.FirstMonthPrice+"&stripe_id="+subscription_id;
    }else{
      
      if(this.cookieService.get('fp_waitlist_loggedin_customer')){
        var encoded =  "?login=true"+checksub+userType+"&signedup=true&selectedPlan="+selectedPlan+"&total_amount="+this.FirstMonthPrice+"&stripe_id="+subscription_id; 
      }else{
        var encoded = "?openlogin="+btoa(email)+checksub+userType+"&signedup=true&selectedPlan="+selectedPlan+"&total_amount="+this.FirstMonthPrice+"&stripe_id="+subscription_id;
      }
    }

    this.data.reset_waitlist_cookies();
    window.location.href = this.data.SHOPIFY_FRONTEND_URL+encoded;
  }
  sentEmail(obj){
    this.data.sentEmailAdim(obj).subscribe(
      data => {
        console.log('email sent')
      })
  }
  giftCodeSubmit(){
    if(this.validGiftCode != 'true'){
       var value = this.PurchasePlanForm.controls['gift_code'].value;
      this.checkGiftCode(value);
    }
   
    return false;
  }
  promoCodeSubmit(){
    var value = this.PurchasePlanForm.controls['ref_code'].value;
    this.checkRefCode(value);
    return false;
  }
}

function noWhitespaceValidator(control: FormControl) {
    let data = control.value;
    if(data.length > 0){
      const isWhitespace = (control.value || '').trim().length === 0;
      const isValid = !isWhitespace;
      return isValid ? null : { 'whitespace': true };
    }
}

function CardExpiryValidator(control: FormControl) {
  let dob = control.value;
  let next_month = now.getMonth()+2;
  let year = now.getFullYear();
  var yearNew = year.toString();
  yearNew = yearNew.substr(-2);
  
      if(dob){
        if(dob.length == 4){
            //console.log("current_month=> "+next_month+" Year=>"+yearNew);
            //console.log(" CardExpiry here.....::  "+dob.length);
            if((dob.substr(0, 2) == next_month || dob.substr(0, 2) < next_month)  && dob.substr(-2) == yearNew){
              //console.log("month is not valid... "+next_month+" Year=>"+yearNew);
              return {
                nextMonthExpiry:{
                  chk:true
                }
              }
            }else{
             /* return {
                nextMonthExpiry:false
              }*/
            }
        } 
    }
  return null;
}
function firstLetterCheck(control: FormControl) {
  let data = control.value;
  if(data.length > 0){
    var firstLetter = data.substr(0,1);
    if(firstLetter == "1"){
      return {
        CheckFirstLetter:{
          chk:true
        }
      }
    }
    return null;
  }
  return null;
}
