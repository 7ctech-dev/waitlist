import { Component, OnInit, Inject, Injectable } from '@angular/core';
import { DOCUMENT } from '@angular/platform-browser';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Router, ActivatedRoute} from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import {formatDate } from '@angular/common';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  STRIPE_PROXY_URL  : string; 
  SHOPIFY_PROXY_URL : string;
  SHOPIFY_FRONTEND_URL : string;
  DBINFOURL         : string;
  Step2Complete     : string;
  ShopifyCustomer   : any;
  CustomerShopifyID : string;
  CustomerStripeID  : string;
  cardError    :string;
  CardErrorMessage:string;
  SocialitePlanPrice    : string;
  TrendsetterPlanPrice  : string;
  WanderlustPlanPrice   : string;
  SocialitePlanDescription    : string;
  TrendsetterPlanDescription  : string;
  WanderlustPlanDescription   : string;
  current_page_url : any;
  today= new Date();
  BLACK_FRIDAY_SALE : any;
  plansSpotsLeft:any
  constructor(  @Inject(DOCUMENT) private document: any, private route: ActivatedRoute,  private cookieService: CookieService, private http:HttpClient, private router: Router,    private spinner: NgxSpinnerService    ) { 
    
    // Enable Black Friday Sale 
    this.BLACK_FRIDAY_SALE = false;

    var domain = this.document.location.hostname;
    var str1 = domain; 
    var str2 = str1.replace('https://', '');
    //var arr = str2.split('.');
    var str3 = str2.replace('.', ' ');
    var index = str3.indexOf( "hautedb" );
    if(str3.indexOf( "hautedb" ) > 0  || domain =='localhost'){
        this.STRIPE_PROXY_URL     = "https://hautedb.com/hautapi/api_stage_3/"; 
        this.SHOPIFY_PROXY_URL    = "https://hautedb.com/hautapi/api_stage_3/";
        this.DBINFOURL            = "https://stagethree.hautedb.com/";
        this.SHOPIFY_FRONTEND_URL = "https://haute-and-borrowed-stage-3.myshopify.com/";
      // this.STRIPE_PROXY_URL     = "https://hautedb.com/hautapi/api_stage_main/"; 
      // this.SHOPIFY_PROXY_URL    = "https://hautedb.com/hautapi/api_stage_main/";
      // this.DBINFOURL            = "https://stagemain.hautedb.com/";
      // this.SHOPIFY_FRONTEND_URL = "https://haute-and-borrowed-stage-main.myshopify.com/";
    }else {
        this.fullstory(null);
        this.STRIPE_PROXY_URL     = "https://data.thefashionpass.com/router/api_live/"; 
        this.SHOPIFY_PROXY_URL    = "https://data.thefashionpass.com/router/api_live/";
        this.DBINFOURL            = "https://data.thefashionpass.com/";
        this.SHOPIFY_FRONTEND_URL = "https://thefashionpass.com/";
    }

    this.Step2Complete        = "false";
    this.SocialitePlanPrice   = "79";
    this.TrendsetterPlanPrice = "109";
    this.WanderlustPlanPrice  = "139";

    this.cardError            = "false";
    this.CardErrorMessage     = "";
    
    this.SocialitePlanDescription    = "Rent 2 clothing items + 1 accessory at a time";
    this.TrendsetterPlanDescription  = "Rent 3 clothing items + 2 accessories at a time";
    this.WanderlustPlanDescription   = "Rent 4 clothing items + 3 accessories at a time";
    this.plansSpotsLeft = {socialite:4,trendsetter:1,wanderlust:2}
  }
  checkAlreadyRegister:any;
  gotoStep:any;

  checkPageView(){ 
    //this.spinner.show();
    this.route.queryParams.subscribe(params => {
        if(params['plan']){
            let planName = params['plan'];
            if( planName.length >0 &&  (planName == "socialite" || planName == "trendsetter" || planName == "wanderlust" )){
                this.reset_waitlist_cookies();
                this.cookieService.set( 'fp_waitlist_selected_plan_web',  planName);
            }
        }else if(params['goto_plans']){
          var allValues = atob(params['goto_plans']);
          if(allValues.length > 0){
              var data = allValues.split("##");
              var email = data[0];
              var shopifyID = btoa(data[1]);
              var stripeID = btoa(data[2]);
              //alert(email+" - "+shopifyID+" - "+stripeID);
              //this.reset_waitlist_cookies();
              this.cookieService.set('fp_waitlist_email',  email);
              this.cookieService.set('fp_waitlist_step',  "plans");
              this.cookieService.set('CustomerStripeID',  stripeID);
              this.cookieService.set('CustomerShopifyID',  shopifyID);  
          }
        } //
        else if(params['vipemail']){
          var cus_email = atob(params['vipemail']);
          this.cookieService.set("fp_waitlist_customer_type", "vip");
          if(cus_email.length > 0){
             if(this.cookieService.get('fp_waitlist_email') &&  this.cookieService.get('fp_waitlist_email')  == cus_email ) {
                  if(this.cookieService.get('fp_waitlist_step') &&  this.cookieService.get('fp_waitlist_step')  == "thankyou" ) {
                    this.cookieService.set('fp_waitlist_step',  "plans");
                  }
             }else{
                this.cookieService.set('fp_waitlist_step',  "waitlist");
                this.cookieService.set('fp_waitlist_check_email',  cus_email);
             }
          }
        }
        else if(params['email']){
          var cus_email = atob(params['email']);
          if(cus_email.length > 0){
             // this.reset_waitlist_cookies();
             
             if(this.cookieService.get('fp_waitlist_email') &&  this.cookieService.get('fp_waitlist_email')  == cus_email ) {
                  if(this.cookieService.get('fp_waitlist_step') &&  this.cookieService.get('fp_waitlist_step')  == "thankyou" ) {
                    this.cookieService.set('fp_waitlist_step',  "plans");
                  }
                  if(window.location.href.indexOf('email_type=') > -1){
                    window.location.href.split('email_type');
                    var url = new URL(window.location.href);
                    var email_type = url.searchParams.get("email_type");
                    this.cookieService.set( 'fp_waitlist_email_type',email_type);
                    var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":email_type,"event":'viewed_select_plan'};
                    console.log(obj);
                    this.setUserActivityBanjo(obj).subscribe(data => {
                      console.log('data responce'+data)
                    });
                    
                  }
             }else{
                // alert('here')
                if(window.location.href.indexOf('email_type=') > -1){
                  // alert('here in')
                  window.location.href.split('email_type');
                  var url = new URL(window.location.href);
                  var email_type = url.searchParams.get("email_type");
                  this.cookieService.set('here121','99');
                  // this.cookieService.set('fp_waitlist_email_type',email_type);
                  localStorage.setItem("fp_waitlist_email_type", email_type);
                  // alert(email_type)
                }
                this.cookieService.set('fp_waitlist_step',  "waitlist");
                this.cookieService.set('fp_waitlist_check_email',  cus_email);
             }
          }
        }
        else if(params['oemail']){
          var cus_email = atob(params['oemail']);
          if(cus_email.length > 0){
             // this.reset_waitlist_cookies();
                this.cookieService.set('fp_waitlist_step',  "waitlist");
                this.cookieService.set('fp_waitlist_check_email',  cus_email);
                this.cookieService.set('black_friday_pause_customer',  "PauseCustomer");
          }
        }
        else if(params['email_wt']){
          var cus_email2 = params['email_wt'];
          if(cus_email2.length > 0){
             // this.reset_waitlist_cookies();
              this.cookieService.set('fp_waitlist_check_email',  cus_email2);
          }
        }
        else if(params['email_check']){
          if(this.cookieService.get('fp_waitlist_check_email_old')){
                this.cookieService.set('fp_waitlist_check_email', this.cookieService.get('fp_waitlist_check_email_old'));
                this.cookieService.delete('fp_waitlist_check_email_old');
          }
          
        }else if(params['code']){
          var code = params['code'];
          if(code.length > 0){
              var NewCode = code.split(":");
              this.cookieService.set('fp_waitlist_apply_code', NewCode[0] );
          }
        }
        if(params['emailSelect']){
            //this.reset_waitlist_cookies();
            var emailSelect = atob(params['emailSelect']);
            this.cookieService.set('fp_waitlist_emailSelect', emailSelect);
            this.cookieService.set( 'fp_waitlist_step',  "waitlist" );
        }

        if(params['loggedin']){
            this.cookieService.set('fp_waitlist_loggedin_customer', 'true');
        }
        
        if(params['loggedin_plan']){
          let planName = params['loggedin_plan'];
          if( planName.length >0 &&  (planName == "socialite" || planName == "trendsetter" || planName == "wanderlust" )){
            this.cookieService.set( 'fp_waitlist_selected_plan_web',  planName);
            this.cookieService.set( 'fp_waitlist_selected_plan',  planName);
            console.log('logged-in-plan');
          }
        }


    });

    this.checkAlreadyRegister = this.cookieService.get('fp_waitlist_email');
    if(this.checkAlreadyRegister){
        this.current_page_url =  this.router.url.substr(1);
        this.gotoStep = this.cookieService.get('fp_waitlist_step');
        
            if(this.current_page_url != this.gotoStep){
              if(this.cookieService.get('fp_waitlist_selected_plan') &&  this.cookieService.get('fp_waitlist_step') == 'plans'){
                  if(window.location.href.indexOf('email_type=') > -1){
                    window.location.href.split('email_type');
                    var url = new URL(window.location.href);
                    var email_type = url.searchParams.get("email_type");
                    this.cookieService.set( 'fp_waitlist_email_type',email_type);
                    var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":email_type,"event":'click_select_plan'};
                    console.log(obj);
                    this.setUserActivityBanjo(obj).subscribe(data => {
                      console.log('data responce'+data)
                    });
                    
                  }
                  this.cookieService.set('fp_waitlist_step',  "purchase-plan");
                  this.router.navigate(["purchase-plan"]);
                }else { 
                  if(window.location.href.indexOf('email_type=') > -1){
                    window.location.href.split('email_type');
                    var url = new URL(window.location.href);
                    var email_type = url.searchParams.get("email_type");
                    this.cookieService.set( 'fp_waitlist_email_type',email_type);
                    var event  = '';
                    if(this.gotoStep == 'purchase-plan'){
                      event ='click_select_plan';
                    }else if (this.gotoStep == 'plans'){
                      event ='viewed_select_plan';
                    }
                    var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":email_type,"event":event};
                    console.log(obj);
                    this.setUserActivityBanjo(obj).subscribe(data => {
                      console.log('data responce'+data)
                    });
                    
                  }
                  this.router.navigate([this.gotoStep]); 

                }
            }else{

            }

    }else{ 
      this.router.navigate(["waitlist"]);
    }
  }
   
  createShopifyCustomer(data){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post<string>(this.SHOPIFY_PROXY_URL+"generic_shopify_request.php", data , { responseType: 'json', headers: headerOptions });
  }
  getShopifyCustomer(email){
    var settings = {
      "url": "/admin/customers.json?email="+email,
      "method": "GET",
      "headers": {
          "content-type": "application/json"
      }
      
    };
    var data = JSON.stringify(settings);
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post<string>(this.SHOPIFY_PROXY_URL+"generic_shopify_request.php", data , { responseType: 'json', headers: headerOptions });
  }
  createBanjoCustomerNew(RequestURL){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.get(this.DBINFOURL+RequestURL , { responseType: 'text', headers: headerOptions });
  }
  sentEmailAdim(obj){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL+"Waitlist/send_email_to_admin",obj, { responseType: 'json', headers: headerOptions });
  }
  getWaitlistTIme(){
    //return "72 HRS";
    // let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    // return this.http.get(this.SHOPIFY_FRONTEND_URL+"pages/current-wait-time", { responseType: 'text', headers: headerOptions });
    return this.http.get('assets/wt-hour.txt', {responseType: 'text'});
    // return data;
  }
  setUserActivityBanjo(obj){
    // var data = JSON.stringify(obj);
    console.log(this.DBINFOURL+"Waitlist/waitlist_customer_activity");
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL+"Waitlist/waitlist_customer_activity",obj, { responseType: 'json', headers: headerOptions });
    // alert('1')
    // return data;
  }
  createBanjoCustomer(RequestURL){
    let headerOptions2 = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    this.http.get(this.DBINFOURL+RequestURL, { responseType: 'text', headers: headerOptions2 }).subscribe(
        data  => {
          console.log("Referral Code is : "+ data);
          if(data){
              if(data.length > 0){
                var expire = new Date();
                expire.setTime(expire.getTime()+(30*24*60*60*1000));
                if(this.cookieService.get( 'fp_waitlist_referrals')){
                    var temp_ref = this.cookieService.get( 'fp_waitlist_referrals'); 
                    var temp_ref2 = temp_ref+", "+data;
                    this.cookieService.set( 'fp_waitlist_referrals',  temp_ref2, expire);
                }else{
                  this.cookieService.set( 'fp_waitlist_referrals',  data, expire);
                }
            }
          }
         },
        error => {
            console.log("Error: Banjo StripeDirect URL.");
            console.log("Error", error);
        });

  }
  
  createWaitlistCustomer(customer_id,CustomerStripeID, customer_email, f_name,l_name){
    var loginAttempt = '';
    if(this.cookieService.get('fp_waitlist_attempt_customer')){
      loginAttempt = "?loginAttempt="+this.cookieService.get('fp_waitlist_attempt_customer');
    }
    var LastCustomerFlag = '';
    if(this.cookieService.get('fp_waitlist_LastCustomerShopifyID')){
      LastCustomerFlag = this.cookieService.get('fp_waitlist_LastCustomerShopifyID');
    }
    let headerOptions2 = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post(this.DBINFOURL+"Waitlist/create_waitlist_customer"+loginAttempt, {
        "first_name" : f_name,
        "last_name" : l_name,
        "customer_shopify_id"  : customer_id,
        "customer_stripe_id"   : CustomerStripeID,
        "email"                : customer_email,
        "LastCustomerFlag"     : LastCustomerFlag
      } , { responseType: 'json', headers: headerOptions2 });
  }

  createStipeAccountM2(customer_email, customer_id,  reloadClear, cname, cbd, bodyt, referral_obj) {

    this.cookieService.set( 'CustomerShopifyID',  btoa(customer_id));
    if(reloadClear == undefined){
      reloadClear= false
    }
    if(cname == undefined){
      cname= "-1";
    }
    if(cbd == undefined){
      cbd= "-1";
    }
    if(bodyt == undefined){
      bodyt= "-1";
    }
    var customer = null;
  
    let body = JSON.stringify({
      "url":  "/customers",
      "method": "POST",
      "headers": {
        "authorization": "Bearer "
      },
      "data" : { "email": customer_email }
    });
    var first_name = cname.split(" ");
    var coupon_id = first_name[0]+"60";
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    
    var postUrlStripeUser = this.STRIPE_PROXY_URL + "generic_stripe_request.php"
    return this.http.post<string>(postUrlStripeUser, body, { responseType: 'json', headers: headerOptions });
  }
 
  checkCustomerCard(last4, card_expiry, zip_code){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + '/Waitlist/checkCustomerCard', {
      "customer_shopify_id" :  this.cookieService.get('CustomerShopifyID'),
      "last4"       :  last4.substr(-4),
      "card_expiry" :  card_expiry,
      "zip_code"    :  zip_code,
    }, { responseType:'json', headers: headerOptions });
  }

  purchasePlanM2(NameOnCard, CardNumber, CardCVV, ZipCode, ExpiryMonth, ExpiryYear, PlanPrice){ 

    this.CustomerStripeID =  atob(this.cookieService.get("CustomerStripeID")); 
    let card_data =JSON.stringify({
        "url": "/customers/" +this.CustomerStripeID+ "/sources",
        "method": "POST",
        "headers": {
            "authorization": "Bearer "
        },
        "data": {
            "source": {
                "object" : "card",
                "exp_month" : ExpiryMonth,
                "exp_year" : ExpiryYear,
                "number" : CardNumber,
                "currency" : "usd",
                "cvc" : CardCVV,
                "name" : NameOnCard,
                "address_country" : "US",
                "address_zip"   : ZipCode,
                "default_for_currency" : "false"
            }
        }
    });

    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');

    //Add Card in stripe account on this request...
    return this.http.post<string>(this.STRIPE_PROXY_URL+"generic_stripe_request.php", card_data  , { responseType: 'json', headers: headerOptions });
  }

  setPhoneNumber(phone_number){

      let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
      return this.http.post<string>(this.DBINFOURL + '/Waitlist/setCustomerPhoneNumber', {
        'customer_email': this.cookieService.get("fp_waitlist_email"),
        "phone_number":phone_number
      }, { responseType:'json', headers: headerOptions });

  }
  addCouponInCustomer(settings){

        let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
        this.http.post<string>(this.DBINFOURL+"waitlist/add_customer_coupon/", settings, { responseType:'json', headers: headerOptions })
        .subscribe(
            data=>{ },
            error=>{ console.log("Error: Card default!"); console.log("Error", error); }
          );

  }
  UpdateWaitlistCustomer(settings,PlanPrice, CardNumber, cardType, coupon, zip_code, city, state,phone_number, card_expiry){
      let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
      this.CustomerShopifyID =  atob(this.cookieService.get("CustomerShopifyID"));
      this.CustomerStripeID  = atob(this.cookieService.get("CustomerStripeID"));
      
      if(settings){
          this.http.post<string>(this.SHOPIFY_PROXY_URL+"generic_stripe_request.php", settings, { responseType:'json', headers: headerOptions })
          .subscribe(
              data=>{ },
              error=>{ console.log("Error: Card default!"); console.log("Error", error); }
            );
      }
      var customerType  = "";
      if(this.cookieService.get('fp_waitlist_vip_customer')){
          customerType  = "vip";
      }
      var PhoneFlag = '';
      if(this.cookieService.get('fp_waitlist_attempt_phone')){
        PhoneFlag  = this.cookieService.get('fp_waitlist_attempt_phone');
      }
      var OrderFlag = '';
      if(this.cookieService.get('fp_waitlist_last_order')){
        OrderFlag  = this.cookieService.get('fp_waitlist_last_order');
      }
      var CardFlag = '';
      if(this.cookieService.get('fp_waitlist_attempt_card')){
        CardFlag  = this.cookieService.get('fp_waitlist_attempt_card');
      }
      
      var bad_customer = '';
      if(this.cookieService.get('fp_waitlist_bad_customer')){
          bad_customer = ",bad_customer";
      }
      var promoCode = '';
      if(this.cookieService.get('fp_waitlist_promoCode')){
        promoCode = this.cookieService.get('fp_waitlist_promoCode');
      }
      var black_friday_sale = "disable";
      if(this.BLACK_FRIDAY_SALE == true){
          black_friday_sale = "enable";
      }

      var zero_hour_bypass = '';
      if(this.cookieService.get('wt_hour') == '0'){
        zero_hour_bypass = "ZERO";
      }else{
        zero_hour_bypass = "noZero";
      }

      return this.http.post<string>(this.DBINFOURL + '/Waitlist/update_waitlist_customer', {
        'customer_email': this.cookieService.get("fp_waitlist_email"),
        "customer_stripe_id":   this.CustomerStripeID ,
        "customer_card_last4":  CardNumber.substr(-4),
        "selected_plan":        this.cookieService.get('fp_waitlist_selected_plan'),
        "plan_value": PlanPrice,
        "cardType": cardType,
        "referral_code": coupon,
        "city": city,
        "state": state,
        "zip_code": zip_code,
        "phone_number":phone_number,
        "customerType":customerType,
        "OrderFlag":OrderFlag,
        "PhoneFlag":PhoneFlag,
        "CardFlag" : CardFlag,
        "card_expiry":card_expiry,
        "bad_customer":bad_customer,
        "black_friday_sale":black_friday_sale,
        "zero_hour_bypass":zero_hour_bypass,
        "promo_code":promoCode,
        "new_member_discount":this.cookieService.get('creditForMembers')
      }, { responseType:'json', headers: headerOptions });
  }

  reset_waitlist_cookies(){
    this.cookieService.delete('fp_waitlist_email');
    this.cookieService.delete('fp_waitlist_step');
    this.cookieService.delete('fp_waitlist_selected_plan');
    this.cookieService.delete('CustomerStripeID');
    this.cookieService.delete('CustomerShopifyID');
    this.cookieService.delete('fp_waitlist_check_email');
    this.cookieService.delete('fp_waitlist_fullname');
    this.cookieService.delete('fp_waitlist_tempvalue');
    //delete all cookies
    var referral          = this.cookieService.get('fp_waitlist_referrals');
    var attempt_customer  = '';
    if(this.cookieService.get('fp_waitlist_attempt_customer')){
      attempt_customer  = this.cookieService.get('fp_waitlist_attempt_customer');
    }
    var lastCustomer = '';
    if(this.cookieService.get('fp_waitlist_LastCustomerShopifyID')){
        lastCustomer = this.cookieService.get('fp_waitlist_LastCustomerShopifyID');
    }
    var bad_customer = '';
    if(this.cookieService.get('fp_waitlist_bad_customer')){
        bad_customer = this.cookieService.get('fp_waitlist_bad_customer');
    }


    this.cookieService.deleteAll();
    var expire = new Date();
    expire.setTime(expire.getTime()+(30*24*60*60*1000));
    if(attempt_customer != ''){
      this.cookieService.set('fp_waitlist_attempt_customer', attempt_customer);
    }
    if(lastCustomer !=''){
      this.cookieService.set('fp_waitlist_LastCustomerShopifyID', lastCustomer);
    }
    if(bad_customer!=''){
        this.cookieService.set('fp_waitlist_bad_customer',bad_customer);
    }
    // console.log(lastCustomer);
    this.cookieService.set( 'fp_waitlist_referrals',  referral , expire);
  }

  checkFriendReferralCode(code){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + '/Waitlist/checkCouponCode/'+code,  { "email" : this.cookieService.get('fp_waitlist_email') ,"plan_name" : this.cookieService.get('fp_waitlist_selected_plan') ,"old_customer" : this.cookieService.get('fp_waitlist_oldCustomer') }, { responseType:'json', headers: headerOptions });
  }
  checkPromoCode(code){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + '/Waitlist/checkPromoCode/'+code,  { "email" : this.cookieService.get('fp_waitlist_email') }, { responseType:'json', headers: headerOptions });
  }
  checkGiftCode(code){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + 'Waitlist/get_gift_coupon',  { "coupon" : code }, { responseType:'json', headers: headerOptions });
  }
  ApplyGiftCode(stripe_id, coupon_id, amount){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + 'Customers_interaction/AddGiftCardCredit',  { "customer_id" : stripe_id,"amount" : amount , "coupon" : coupon_id  }, { responseType:'json', headers: headerOptions });
  }
  check30CreditOn(){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.get(this.DBINFOURL+"Interactions/getDollar30Settings", { responseType: 'text', headers: headerOptions });
    // return this.http.get(this.DBINFOURL +'discount_credit_status.txt', {responseType: 'text'});
  }
  checkCustomerPromo(){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    return this.http.get(this.DBINFOURL+"waitlist/checkCustomerPromotion?email="+this.cookieService.get('fp_waitlist_email'), { responseType: 'text', headers: headerOptions });
  }
  /*getCustomerAccountCredit(email){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.post<string>(this.DBINFOURL + '/Waitlist/checkCouponCode/'+email, {}, { responseType:'json', headers: headerOptions });
  }*/
  getTimeBetween2Dates(time1,time2){
    let timediff = time2.getTime() - time1.getTime(); 
    let minutes = Math.floor((timediff/1000)/60);
    let seconds = Math.floor(timediff/ 1000);
    let hours = Math.abs(time1 - time2) / 36e5;
    hours = Math.trunc( hours );
  
    let obj = {minutes:minutes,hours:hours,seconds:seconds}
    return obj
  }
  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
  }
  planSpotRemainingFunc(number){
    let time = this.cookieService.get('planSpotsRamdomNumberTIme')
    let _this = this
    if(time != ''){
      const newTime = new Date(time)
      let diffObj = this.getTimeBetween2Dates(newTime,new Date())
      console.log(diffObj)
      if(diffObj.hours == 0 && diffObj.minutes <= 1){
        console.log('before 2 mins condition')
        let socialite = 0;
        socialite = number-3
        let secondsLeft = (120 - diffObj.seconds)
        secondsLeft = secondsLeft*1000;
        this.plansSpotsLeft.socialite = socialite
        
        setTimeout(function(){
            _this.planSpotRemainingFunc(_this.cookieService.get("planSpotsRamdomNumber"))
        }, secondsLeft);
      }else if (diffObj.hours == 0 && diffObj.minutes <= 6){
        console.log('after 2 mins condition')
        let socialite = 0;
        socialite = number-3
        let secondsLeft = (420 - diffObj.seconds)
        secondsLeft = secondsLeft*1000;
        this.plansSpotsLeft.socialite = socialite-1
        setTimeout(function(){
            _this.planSpotRemainingFunc(_this.cookieService.get("planSpotsRamdomNumber"))
        }, secondsLeft);
      }else if (diffObj.hours == 0 && diffObj.minutes > 4 && diffObj.minutes < 26){
        console.log('after 5 min condition')
        let socialite = 0;
        socialite = number-4
        let secondsLeft = (1560 - diffObj.seconds)
        secondsLeft = secondsLeft*1000;
        this.plansSpotsLeft.socialite = socialite
        this.plansSpotsLeft.wanderlust = '1'
        setTimeout(function(){
            _this.planSpotRemainingFunc(_this.cookieService.get("planSpotsRamdomNumber"))
        }, secondsLeft);
      }else if (diffObj.hours > 7){
        console.log('8 hours condition')
        this.cookieService.delete("planSpotsRamdomNumberTIme");
        this.cookieService.delete("planSpotsRamdomNumber");
        number = this.getRandomIntInclusive(6,9)
        document.cookie = "planSpotsRamdomNumber="+number+";domain=.thefashionpass.com";
        this.planSpotRemainingFunc(number)
      }else{
        console.log('after 20 min condition')
        if(number > 5){
          this.cookieService.delete("planSpotsRamdomNumber");
          number = this.getRandomIntInclusive(2,4)
          document.cookie = "planSpotsRamdomNumber="+number+";domain=.thefashionpass.com";
        }
        
        let socialite = 0;
        socialite = number-1
        this.plansSpotsLeft.socialite = socialite
        this.plansSpotsLeft.wanderlust = '0'
        this.plansSpotsLeft.trendsetter = '1'
      }
      
    }else{
      document.cookie = "planSpotsRamdomNumber="+number+";domain=.thefashionpass.com";
      document.cookie = "planSpotsRamdomNumberTIme="+new Date()+";domain=.thefashionpass.com";
      // this.cookieService.set("planSpotsRamdomNumber",number.toString());
      // this.cookieService.set("planSpotsRamdomNumberTIme",new Date().toString());
      let socialite = 0;
      socialite = number-3;
      this.plansSpotsLeft.socialite = socialite
      this.plansSpotsLeft.trendsetter = 1
      this.plansSpotsLeft.wanderlust = 2
      setTimeout(function(){
          _this.planSpotRemainingFunc(_this.cookieService.get("planSpotsRamdomNumber"))
        }, 12100);
      console.log('2 min condition')
    }
    let totalSpotsLeft = parseInt(this.plansSpotsLeft.socialite) + parseInt(this.plansSpotsLeft.wanderlust) + parseInt(this.plansSpotsLeft.trendsetter) ;
    let numbers = totalSpotsLeft.toString()
    setTimeout(() => {
      if(document.getElementsByClassName('numberBox')[0] != undefined){
      document.getElementsByClassName('numberBox')[0].innerHTML = numbers
      document.getElementsByClassName('numberBox')[1].innerHTML = numbers
    }
    }, 1000);
    let plansSpotsLeft =  JSON.stringify(this.plansSpotsLeft)
    this.cookieService.set('plansSpotsLeft', plansSpotsLeft);
    if(window.location.href.indexOf('/plans') > -1 && this.cookieService.get('fp_waitlist_email')){
      // this.router.navigate(["plans"]);
      document.getElementsByClassName('wanderlust')[0].innerHTML = '<b>'+this.plansSpotsLeft.wanderlust+'</b> '+(this.plansSpotsLeft.wanderlust == 1 ?'spot left':'spots left')
      document.getElementsByClassName('trendsetter')[0].innerHTML = '<b>'+this.plansSpotsLeft.trendsetter+'</b> '+(this.plansSpotsLeft.trendsetter == 1 ?'spot left':'spots left')
      document.getElementsByClassName('socialite')[0].innerHTML = '<b>'+this.plansSpotsLeft.socialite+'</b> '+(this.plansSpotsLeft.socialite == 1 ?'spot left':'spots left')
    }
  }
  gotoMainSite(){
    var redirectURL = "";
    if(this.cookieService.get('fp_waitlist_email') && this.cookieService.get('CustomerShopifyID') &&  this.cookieService.get('CustomerStripeID') ){ 
      var selectedPlan = this.cookieService.get('fp_waitlist_selected_plan');
      var email = this.cookieService.get('fp_waitlist_email');
      if( this.cookieService.get('fp_waitlist_tempvalue')){
        var pass  = atob(this.cookieService.get('fp_waitlist_tempvalue'));
        var encoded = btoa(email+"(##)"+pass);
        var fname = btoa(this.cookieService.get('fp_waitlist_first_name'));
        redirectURL = this.SHOPIFY_FRONTEND_URL+"collections/clothing?login="+encodeURIComponent(encoded)+"&fname="+fname+"&selectedPlan="+selectedPlan;
        this.cookieService.delete('fp_waitlist_tempvalue');
      }else{
        if(this.cookieService.get('fp_waitlist_loggedin_customer')){
          redirectURL = this.SHOPIFY_FRONTEND_URL+'collections/clothing';
        }else{
          redirectURL = this.SHOPIFY_FRONTEND_URL+"collections/clothing?openlogin="+btoa(email);
        }
      }
    }else{
        redirectURL = this.SHOPIFY_FRONTEND_URL+'collections/clothing'
    }
    var fp_waitlist_email = this.cookieService.get('fp_waitlist_email');
    //this.reset_waitlist_cookies();

    if(this.router.url.substr(1)== "complete-profile"){
        this.cookieService.set('fp_waitlist_check_email_old', fp_waitlist_email);
    }
    this.cookieService.delete('fp_waitlist_loggedin_customer');
    if(this.cookieService.get('fp_waitlist_step') != undefined && this.cookieService.get('fp_waitlist_step') != ''){
      if(redirectURL.indexOf('?openlogin') > -1)
        redirectURL = redirectURL+'&wt-customer=true';
      else
        redirectURL = redirectURL+'?wt-customer=true';
    }
    window.location.href = redirectURL
  }

  verifyVipWaitlist(){
    var CustomerShopifyID = this.cookieService.get('CustomerShopifyID');
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.get<string>(this.DBINFOURL + 'waitlist/verify_VIPWaitlistTag/'+atob(CustomerShopifyID), { responseType:'json', headers: headerOptions });
  }
  IP_MongoDBCheckRequest(ip){
    var jstoday = formatDate(this.today, 'hh:mm dd/MM/yyyy', 'en-US');
    var username = this.cookieService.get('fp_waitlist_fullname');
    var email = this.cookieService.get('fp_waitlist_email');
    var cusID = this.cookieService.get('CustomerShopifyID');
    var zero_hour_bypass = '';
    if(this.cookieService.get('wt_hour') == '0'){
      zero_hour_bypass = "ZERO";
    }else{
      zero_hour_bypass = "noZero";
    }
    var paras = "?ip="+ip+"&email="+email+"&time="+jstoday+"&username="+username;
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    this.http.get<string>(this.SHOPIFY_PROXY_URL + 'mongo_angular_data.php/'+paras, { responseType:'json', headers: headerOptions }).subscribe(data => {
        if(data!=""){
          var response = (data);
          this.http.post<string>(this.DBINFOURL+'waitlist/SetCustomerIPFlag', {
            "email" : data,
            "cusId" : cusID,
            "ip" : ip,
            "ipemail" : email,
            "zero_hour_bypass":zero_hour_bypass
          } , { responseType: 'json', headers: headerOptions }).subscribe(data => {
              if(data!=""){
                        var response = (data);
              }
            this.http.get<string>(this.DBINFOURL + 'waitlist/insertCustomerLinkedAccountGeneral/?email='+email, { responseType:'json', headers: headerOptions }).subscribe(data => {});
          });
        }
      });

  }
  getWaitlistCredit(email){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.get<string>(this.DBINFOURL + 'waitlist/get_waitlist_credits/?email='+btoa(email), { responseType:'json', headers: headerOptions });
  }
  getCustomerStripeCard(){
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; text/html;  charset=UTF-8');
    return this.http.get<string>(this.DBINFOURL + 'waitlist/getCustomerStripeCard/'+(this.cookieService.get('CustomerStripeID')), { responseType:'json', headers: headerOptions });
  }

  fullstory(data){
    console.log('FullStory tracking...');
    // only for LIVE store
   
      if(data != null){
        if(window.location.href.indexOf('waitlist.thefashionpass.com') > -1){
          window['FS'].setUserVars({
            displayName: data.first_name+' '+data.last_name,
            email: data.email,
            reviewsWritten_int: 14,
          });
        }
      }else{
        window['_fs_debug'] = false;
        window['_fs_host'] = 'www.fullstory.com';
        window['_fs_org'] = '3SMNC';
        window['_fs_namespace'] = 'FS';
       
        (function(m,n,e,t,l,o,g,y){
          if (e in m && m.console && m.console.log) { m.console.log('FullStory namespace conflict. Please set window["_fs_namespace"].'); return;}
          g=m[e]=function(a,b){g.q?g.q.push([a,b]):g._api(a,b);};g.q=[];
          o=n.createElement(t);o.async=1;o.src='https://www.fullstory.com/s/fs.js';
          y=n.getElementsByTagName(t)[0];y.parentNode.insertBefore(o,y);
          g.identify=function(i,v){g(l,{uid:i});if(v)g(l,v)};g.setUserVars=function(v){g(l,v)};
          g.identifyAccount=function(i,v){o='account';v=v||{};v.acctId=i;g(o,v)};
          g.clearUserCookie=function(c,d,i){if(!c || document.cookie.match('fs_uid=[`;`]*`[`;`]*`[`;`]*`')){
            d=n.domain;while(1){n.cookie='fs_uid=;domain='+d+
              ';path=/;expires='+new Date(0).toUTCString();i=d.indexOf('.');if(i<0)break;d=d.slice(i+1)}}};
        })(window,document,window['_fs_namespace'],'script','user');
      }
    
  }
}
