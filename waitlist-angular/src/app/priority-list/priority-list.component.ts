import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Router} from '@angular/router';
import { DataService } from '../data.service';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any;
@Component({
  selector: 'app-priority-list',
  templateUrl: './priority-list.component.html',
  styleUrls: ['./priority-list.component.css']
})
export class PriorityListComponent implements OnInit {
  waitTime:string;
  waitTimeHours:number;
  constructor( private cookieService: CookieService, private http:HttpClient, private router: Router,private data:DataService) { }

  ngOnInit() {
    //alert();
    new setFBPixel();
    this.data.checkPageView();
    var userId  = this.cookieService.get('CustomerShopifyID');
    this.cookieService.set('fp_waitlist_LastCustomerShopifyID', userId);
      // var postUrlBanjo = "interactions/getWaitlistTime";              
    this.data.getWaitlistTIme()
    .subscribe(data => {
      console.log(data);
      if(data.indexOf('HRS') > -1){
        this.waitTimeHours = parseInt(data.split(' ')[0])
     }else{
      this.waitTimeHours = 0;
     }
      this.waitTime = data;
      this.cookieService.set( 'wt_hour',  data);
      if(this.waitTime == '0'){
        this.gotoReward()
      }
    })
    
   
    // alert('here')
    // this.waitTime = "72 HRS";
  }
  gotoReward(){

    var email   = this.cookieService.get('fp_waitlist_email');
    var pass    = atob(this.cookieService.get('fp_waitlist_tempvalue'));
    
    if(this.cookieService.get('fp_waitlist_tempvalue')){
      var fname = btoa(this.cookieService.get('fp_waitlist_first_name')); ;
      var encoded = "?login="+btoa(email+"(##)"+pass)+"&fname="+fname+"&signedup=true";
    }else{
      
      if(this.cookieService.get('fp_waitlist_loggedin_customer')){
        var encoded =  ""; 
      }else{
        var encoded = "?openlogin="+btoa(email)+"&signedup=true";
      }
    }

    this.data.reset_waitlist_cookies();
    if(this.waitTime == '0'){
      window.location.href = this.data.SHOPIFY_FRONTEND_URL+encoded;
    }else{
        window.location.href = this.data.SHOPIFY_FRONTEND_URL+"pages/viprewards"+encoded;
    }
  }
}
