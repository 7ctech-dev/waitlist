import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { DataService } from '../data.service';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {  CountdownComponent  } from 'ngx-countdown';
import { Pipe, PipeTransform } from '@angular/core';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any;

@Pipe({name: 'titlecase', pure: false})

@Component({
  selector: 'app-complete-profile',
  templateUrl: './complete-profile.component.html',
  styleUrls: ['./complete-profile.component.css']
})

export class CompleteProfileComponent implements OnInit {
  httpClient: any;
  @ViewChild('countdown') counter: CountdownComponent; 
  @ViewChild('ProfileFormSigninForm') ProfileFormSigninForm : ElementRef;
  @ViewChild('tempEmailAddress') tempEmailAddress : ElementRef;


  constructor(
    private cookieService: CookieService,
    private data:DataService, 
    private router: Router,
    private http:HttpClient,
    private spinner: NgxSpinnerService,
    private formBuilder: FormBuilder, 
    private el: ElementRef
    ) { }

  ProfileForm: FormGroup;
  ProfileFormSignin: FormGroup;
  first_name:string;
  last_name:string;
  dress_size:string;
  top_size:string;
  customer_survey:string;
  birthday: string;
  email:string;
  FiltersSettings:string;
  inviteZero:string;
  IP_ADDRESS:any;
  newCustomerID:any;
  customerDetail:any;
  customer_email:string;
  //password:string;
  dob:any;
  ShopifyCustomer:any; 
  showError:any;
  showProfileFormSignin:any;
  SubmitButtonText:any;
  customer_type:any;
  checkCustomerReferalCode : any;    
  waitTime : string;
  phone_number_error:string;
  counterLeftTime: any;
  surveyOptions: any;
  showValidationError:string;
  showOtherSurvey:boolean;
  checkCustomerPromo:any;
  ngOnInit() { 
    new setFBPixel();
    this.showError = false;
    this.showProfileFormSignin= false;
    this.showValidationError = "false";
    this.showOtherSurvey = false;
    this.phone_number_error = 'false';
    this.checkCustomerPromo = 'false';
    // this.SubmitButtonText = "Join the waitlist";
    this.SubmitButtonText = "CREATE PROFILE";
    this.customer_type = "general";
    this.checkCustomerReferalCode = "false";
    if(this.cookieService.get("fp_waitlist_customer_type")){
        if(this.cookieService.get("fp_waitlist_customer_type")=="vip"){
          this.SubmitButtonText = "CREATE PROFILE";
          this.customer_type = "vip";
        }
    }
  
    this.customer_email = this.cookieService.get('fp_waitlist_email');
    console.log(this.customer_email);
    this.http.get("https://api.ipify.org/?format=json").subscribe(
          data => {
               this.IP_ADDRESS = data['ip'];
          }
      );
    //   this.http.get("https://data.thefashionpass.com/users/get_customer_survey").subscribe(
    //     data => {
    //          this.surveyOptions = data;
    //     }
    // );
    // this.data.checkCustomerPromo().subscribe(data => {
    //   console.log(data +" check fp_waitlist_checkCustomerPromot ====-------");
    //   this.cookieService.set('fp_waitlist_checkCustomerPromo', data);
    //   // if(data == 'true'){
    //   //   location.reload();
    //   // }
    // })
    // console.log(this.surveyOptions)
    this.data.checkPageView();
    const expiryRegex = '^(((0)[1-9])|((1)[0-2]))([0-2][1-9]|(3)[0-1]|[1-2][0])(19[5-9][0-9]|20[0][0-9]|2010)$';
    this.ProfileForm = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.pattern('^[a-zA-Z\\s]+$'), noWhitespaceValidator]],
      last_name:  ['', [Validators.required, Validators.pattern('^[a-zA-Z\\s]+$'), noWhitespaceValidator]],
      // customer_survey:   ['', [Validators.required ]],//Validators.pattern('^[a-zA-Z]+$')
      // phone_number:   ['', [Validators.required ]],//Validators.pattern('^[a-zA-Z]+$')
      birthday:   ['',[Validators.required,Validators.maxLength(8), Validators.pattern(expiryRegex)]],
      password:   ['', [Validators.required]] 

    });

    this.ProfileFormSignin = this.formBuilder.group({
      email: ['', [Validators.required]],
      password:   ['', [Validators.required]]
    });
    this.data.getWaitlistTIme()
    .subscribe(data => {
      console.log(data);
      this.waitTime = data;
      this.cookieService.set( 'wt_hour',  data);
    }) 
    if(window.innerWidth > 767) {
      document.getElementById("first_name").focus();}
  }
  showFinishMessage(){
    setTimeout(() => this.counter.restart());
  }
  surveyOptionsChange(deviceValue){
    if(deviceValue.selectedOptions[0].text == 'Other'){
      this.showOtherSurvey = true;
      this.ProfileForm.controls["customer_survey"].setValidators(null)
      setTimeout(function(){document.getElementById('customer_survey').focus()},100)
      // const expiryRegex = '^(((0)[1-9])|((1)[0-2]))([0-2][1-9]|(3)[0-1]|[1-2][0])(19[5-9][0-9]|20[0][0-9]|2010)$';
      // this.ProfileForm = this.formBuilder.group({
      //   first_name: [this.ProfileForm.value.first_name, [Validators.required, Validators.pattern('^[a-zA-Z\\s]+$'), noWhitespaceValidator]],
      //   last_name:  [this.ProfileForm.value.last_name, [Validators.required, Validators.pattern('^[a-zA-Z\\s]+$'), noWhitespaceValidator]],
      //   phone_number: [this.ProfileForm.value.phone_number, [Validators.required ]],//Validators.pattern('^[a-zA-Z]+$')
      //   birthday:   [this.ProfileForm.value.birthday,[Validators.required,Validators.maxLength(8), Validators.pattern(expiryRegex)]],
      //   password:   [this.ProfileForm.value.password, [Validators.required]] ,
      //   customer_survey:   [this.ProfileForm.value.customer_survey, [Validators.nullValidator]] 
  
      // });
    }else{
      this.showOtherSurvey = false
    }
  }
  
  CharactersOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    //if(!(charCode >= 65 && charCode <= 120) && (charCode != 0)){ return false;    }
    if((charCode >= 65 && charCode <= 90) || (charCode >= 95 && charCode <= 122) || charCode == 32) {

    }else{
      return false;
    }
    return true;
  }
  NumbersOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
     if (charCode > 31 && (charCode < 48 || charCode > 57)) {  return false; }
    return true;
  }
 
  gotoThankyou() {
    this.router.navigate(['thankyou']);
  }
  
  submitProfile(){ 
     
    
    if (this.ProfileForm.valid) { 
 
      this.spinner.show();
      this.http.get("https://api.ipify.org/?format=json").subscribe(
          data => {
              this.IP_ADDRESS = data['ip'];
          }
      );

      this.ProfileFormSignin.setValue({
        email:    this.cookieService.get('fp_waitlist_email'),
        password: this.ProfileForm.value.password.trim()
      });
      this.showProfileFormSignin= true;

      this.first_name = this.transform(this.ProfileForm.value.first_name.toLowerCase().trim());
      this.last_name  = this.transform(this.ProfileForm.value.last_name.toLowerCase().trim());

      this.cookieService.set( 'fp_waitlist_first_name',  this.first_name);

      this.cookieService.set( 'fp_waitlist_tempvalue',  btoa(this.ProfileForm.value.password.trim()));
      var device_width = window.innerWidth;
      var mob_web = "";
      if (device_width < 500){ mob_web= "Mobile Signup"; }
      else{ mob_web= "Web Signup"; }

      this.FiltersSettings='FilterSettings:0';
      this.inviteZero ='invite0';  
      let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      var NewBirthday = this.ProfileForm.value.birthday;
      NewBirthday   = NewBirthday.substr(0,2)+"/"+NewBirthday.substr(2,2)+"/"+NewBirthday.substr(4,4);
 
      this.cookieService.set("fp_waitlist_fullname",this.first_name+" "+this.last_name);
      var bad_customer = '';
      if(this.cookieService.get('fp_waitlist_bad_customer')){
          bad_customer = ",bad_customer";
      }
      
      this.cookieService.set( 'fp_waitlist_phone_no',  this.ProfileForm.value.phone_number);
      // this.data.setPhoneNumber(this.ProfileForm.value.phone_number).subscribe(
      //   PhoneResponse =>{ 
      //     if(PhoneResponse.toString() == "true" || this.cookieService.get('fp_waitlist_vip_customer') ){
            let ShopifyData = JSON.stringify({
              "url": "/admin/customers.json",
              "method": "POST",
              "headers": {
                "content-type": "application/json"
              },
              "data": {
                "customer": {
                  "first_name": this.first_name,
                  "last_name": this.last_name,
                  "email": this.cookieService.get('fp_waitlist_email'),
                  "verified_email": true,
                  "password": this.ProfileForm.value.password.trim(),
                  "password_confirmation": this.ProfileForm.value.password.trim(),
                  "send_email_welcome": true,
                  "tags": "Birthday:" + NewBirthday + ",Profile IP: " + this.IP_ADDRESS+","+mob_web+","+this.FiltersSettings+","+this.inviteZero+",AllowDebit , waitlistjoined"+bad_customer
                  // "tags": "Birthday:" + NewBirthday + ",Dress Size:"+this.ProfileForm.value.dress_size2+':'+this.ProfileForm.value.dress_size+", Profile IP: " + this.IP_ADDRESS+","+mob_web+","+this.FiltersSettings+","+this.inviteZero+",AllowDebit ,Shirt & Top: "+this.ProfileForm.value.top_size+", waitlistjoined"+bad_customer
                }
              }
            });
            var full_name_for_stripe = this.first_name+"_ln_"+this.last_name;
            this.data.createShopifyCustomer(ShopifyData).subscribe(
                data => {
                    console.log("Shpify response: "+data);
                    this.customerDetail = data;
                    var newData =   JSON.parse(this.customerDetail);
                    this.data.fullstory(newData.customer)
                    if(newData.customer){
                      var customerDetail = newData.customer;
                      this.newCustomerID = customerDetail.id;
                      this.data.createStipeAccountM2(this.cookieService.get('fp_waitlist_email'), this.newCustomerID, false, full_name_for_stripe,NewBirthday,'','')
                      .subscribe(
                        response => {
                            var bodyt= ''; var coupon_id = '';
                            this.ShopifyCustomer =   response;
                            var CustomerStripeID =  this.ShopifyCustomer.id.trim();
                            this.cookieService.set( 'CustomerStripeID',  btoa(CustomerStripeID));
                            var postUrlBanjo = "Customer_data_endpoint/stripeDirect?email="+this.cookieService.get('fp_waitlist_email')+"&id="+CustomerStripeID+"&cname="+full_name_for_stripe+"&cbd="+NewBirthday+"&bodyt="+bodyt+"&coupon_id="+coupon_id+"&req=stripeapifunctions132&shopifyid="+this.newCustomerID+'&survey='+this.ProfileForm.value.customer_survey+'&phone='+this.ProfileForm.value.phone_number;
                            
                            this.data.createBanjoCustomerNew(postUrlBanjo)
                              .subscribe(data => {
                                
                                if(data){}
                                  if(data.length > 0){
                                      console.log("response =>>>> 1 "+data);
                                      this.checkCustomerReferalCode = "true";
                                      console.log(this.checkCustomerReferalCode);
                                      var expire = new Date();
                                      expire.setTime(expire.getTime()+(30*24*60*60*1000));
                                      if(this.cookieService.get( 'fp_waitlist_referrals')){
                                          var temp_ref = this.cookieService.get( 'fp_waitlist_referrals'); 
                                          var temp_ref2 = temp_ref+", "+data;
                                          this.cookieService.set( 'fp_waitlist_referrals',  temp_ref2, expire);
                                      }else{
                                        this.cookieService.set( 'fp_waitlist_referrals',  data, expire);
                                      }
                                  }

                                    if(CustomerStripeID){
                                      this.data.createWaitlistCustomer(this.newCustomerID,CustomerStripeID, this.cookieService.get('fp_waitlist_email'), this.first_name, this.last_name)
                                      .subscribe(data => {
                                        if(data=="1"){
                                          this.data.IP_MongoDBCheckRequest(this.IP_ADDRESS);
                                          
                                          //this.cookieService.set( 'fp_waitlist_step',  "thankyou" );
                                          //this.router.navigate(['thankyou']); 
                                            setTimeout(() => {
                                              this.ProfileFormSigninForm.nativeElement.click();
                                            }, 200);
                                        }
                                      },
                                      error => {
                                          console.log("Error: Customer not added in Waitlist Table.");
                                          console.log("Error", error);
                                          this.spinner.hide();
                                      });
                                    }

                              },error => {
                                console.log("Error: Banjo StripeDirect URL Error.");
                                console.log("Error", error);
                                this.spinner.hide();
                            }); 


                        },
                        error => {
                            console.log("Error: Stripe account  Not creating!");
                            console.log("Error", error);
                        }
                      );
                      
                    }else {
                        console.log("Error: Customer account  Not creating on shopify!");
                        this.cookieService.set('fp_waitlist_check_email',  this.cookieService.get('fp_waitlist_email'));
                        this.cookieService.set('fp_waitlist_step',"waitlist");
                        this.router.navigate(['waitlist']);
                    }
                },
                error => {
                    console.log("Error: Shopify account Not creating!");
                    console.log("Error", error);
                }
            );
      //     }else if(PhoneResponse.toString() == "false") {
      //       this.showValidationError = "true";
      //       this.phone_number_error = "Phone number is invalid."; 
      //       this.spinner.hide();
      //     }else{
      //       this.cookieService.set('fp_waitlist_attempt_phone',this.ProfileForm.value.phone_number);
      //       this.showValidationError = "true";
      //       this.phone_number_error = "Phone number "+PhoneResponse.toString()+"."; 
      //       this.spinner.hide();
      //     }
      //   }
      // )
     
    }else {
      this.showError = true;
    }
  }


  submitEmailAddress(){
    if (this.ProfileFormSignin.valid) {
        if(this.customer_type == "vip" || this.waitTime == '0'){
            this.cookieService.set( 'fp_waitlist_step',  "plans" );
            this.router.navigate(['plans']);
        }else{
            this.cookieService.set( 'fp_waitlist_step',  "thankyou" );
            this.router.navigate(['thankyou']);
        }
      }
  }

  transform(input: string): string {
    return input.length === 0 ? '' :
      input.replace(/\w\S*/g, (txt => txt[0].toUpperCase() + txt.substr(1).toLowerCase() ));
  }
  clearPhoneError(){
    this.phone_number_error = "false";
  }
  // countdown_restart(){
  //   this.counterLeftTime = 300;
  //   setTimeout(() => this.counter.restart()); 
  // }
  SetPhoneValidation(event): boolean {
    
    const charCode = (event.which) ? event.which : event.keyCode;
    var first  = event.target.value;
    var second = "";
    if(first.length > 0){
       second = "(";
    }
    first  = first.substr(1,1);
    var last = first.substr(2);
    
    if(first == '1'){
      (<HTMLInputElement>document.getElementById("phone_number")).value = second+last;
      console.log(event.target.value);
      return false;
    }
    //else*/
    //if (charCode > 31 && (charCode < 48 || charCode > 57 || first == '1')   ) {  return false; }
    return true;
  }
}

function dobValidator(control: FormControl) {
  let dob = control.value;
  console.log(" dob here.....::  ");
      if(dob){
        
        if(dob.substr(0, 2) >= 1 && dob.substr(0, 2) <=12 ){
            console.log('Month: ' + dob.substr(0, 2))
        }else{

        if(dob.substr(0, 2) != ''){
          return {
            dobMonth:{
                parsedDob: dob.substr(0, 2)
            }
          }
          }
        }
        
        if(dob.substr(2, 2) >= 1 && dob.substr(2, 2) <=31 ){
            console.log('Days: ' + dob.substr(2, 2))
        }else{
            if(dob.substr(2, 2) != ''){
              return {
                dobDays:{
                    parsedDob: dob.substr(2, 2)
                }
              }
            }
        }
        
        if(dob.substr(4, 8) >= 1950 && dob.substr(4, 8) <=2010 ){
            console.log('Year: ' + dob.substr(4, 8))
        }else{

          if(dob.substr(4, 8) != ''){
            return {
              dobYear:{
                parsedDob: dob.substr(4, 8)
              }
            }
          }

        } 
    }
  return null;
  }
  function noWhitespaceValidator(control: FormControl) {
    let data = control.value;
    if(data){
      if(data.length > 0){
        const isWhitespace = (control.value || '').trim().length === 0;
        const isValid = !isWhitespace;
        return isValid ? null : { 'whitespace': true };
      }
    }
}
