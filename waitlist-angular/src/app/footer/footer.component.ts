import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router} from '@angular/router';
import { DataService } from '../data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor(   private cookieService: CookieService,
    private data:DataService,
    private router: Router) { }

  showFooter:any;  
  lastOrderCheck:any;
  ngOnInit() {
    this.showFooter = "false";
    this.lastOrderCheck = 'false';
    if(this.cookieService.get('fp_waitlist_step') == "purchase-plan"){
        this.showFooter = "true";
    }
    if(this.cookieService.get('fp_waitlist_last_order')){
      this.lastOrderCheck = 'true';
    }
  }

}
