import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router} from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders,HttpParams} from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-waitlist',
  templateUrl: './waitlist.component.html',
  styleUrls: ['./waitlist.component.css']
})
export class WaitlistComponent implements OnInit {

  EmailForm:FormGroup;
  email:string;
  expandedError:any;
  invalidEmailError:any;
  showError:any;
  showValidationError:any;
  
  constructor(
    private cookieService: CookieService,
    private data:DataService, 
    private router: Router,
    private http:HttpClient,
    private formBuilder: FormBuilder,
    private spinner: NgxSpinnerService
  ) { }

 myEmail:string;
  ngOnInit() {
    this.showValidationError = false;
    this.showError = false;
    //this.spinner.show();
    //setTimeout(() => { this.spinner.hide(); }, 5000);
    this.data.checkPageView(); 
    this.expandedError = false;
    this.invalidEmailError = false;

    if(this.cookieService.get('fp_waitlist_check_email')){
        this.spinner.show();
        this.sendEmailRequest(this.cookieService.get('fp_waitlist_check_email'));
    }
    const email_regex = '^([a-zA-Z0-9_\.\-]{2,})+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9])+$'; 
    this.EmailForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email, Validators.pattern(email_regex), firstLetterCheck]],
    })
  }

  checkError(){
    this.expandedError = false;
    this.invalidEmailError = false;
  }
  hideError(){
    this.showValidationError = false;
    this.expandedError = false;
    this.invalidEmailError = false;
  }
  gotoCompleteProfile() {
    this.router.navigate(['complete-profile']);
  }
  submitEmailAddress(){
    

    if (this.EmailForm.valid) {
      this.invalidEmailError = false;
      this.expandedError = false;
      this.spinner.show();
      this.email = this.EmailForm.value.email;
      this.sendEmailRequest(this.email);
    }else{
      this.showError = true;
      this.showValidationError = true;
      this.invalidEmailError = false;
    }
  }
  sendEmailRequest(customer_email){
    
    this.email = customer_email;
    let FormData = "email="+customer_email;
    let headerOptions = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    this.http.post(this.data.DBINFOURL+"Waitlist/checkWaitlistEmail/", FormData , { responseType: 'text', headers: headerOptions })
    .subscribe(
        data => {
            var fp_waitlist_selected_plan = '';
            if(this.cookieService.get('fp_waitlist_selected_plan_web')){ //&&  logedInUser == "true"
                fp_waitlist_selected_plan =  this.cookieService.get('fp_waitlist_selected_plan_web');
            } 
             if(data == "true"){ 
                var expire = new Date();
                //var time = Date.now() + ((3600 * 1000) * 6); // current time + 6 hours ///
                expire.setTime(expire.getTime()+(30*24*60*60*1000));
                this.data.reset_waitlist_cookies();
                if(fp_waitlist_selected_plan!=''){
                  this.cookieService.set('fp_waitlist_selected_plan', fp_waitlist_selected_plan);
                }
                this.cookieService.set( 'fp_waitlist_email',  this.email, expire);
                this.cookieService.set( 'fp_waitlist_step',  "complete-profile" );
                this.router.navigate(['complete-profile']);
             }else if(data == "false"){
                this.expandedError = true;
             }else if(data == "invalid"){
                if(this.cookieService.get('fp_waitlist_check_email')){
                    this.EmailForm.setValue({email:this.cookieService.get('fp_waitlist_check_email')});
                }
                this.invalidEmailError = true;
             }else if(data.indexOf("shopifysuer") !=  -1){
                  var splitData = data.split(":");
                  var customerAtemp = splitData[1];
                  this.cookieService.delete('fp_waitlist_check_email');
                  this.cookieService.set('fp_waitlist_attempt_customer', customerAtemp);
                  window.location.href =  this.data.SHOPIFY_FRONTEND_URL+"?openlogin="+btoa(this.email);
             }
             else{
                this.data.reset_waitlist_cookies();
                if(fp_waitlist_selected_plan!=''){
                  this.cookieService.set('fp_waitlist_selected_plan', fp_waitlist_selected_plan);
                }
                this.cookieService.delete('fp_waitlist_check_email');
                var responseData = JSON.parse(data);
                

                    if(responseData.LastOrderID){
                      this.cookieService.set('fp_waitlist_last_order', responseData.LastOrderID);
                    }
                    if((responseData.customer_shopify_id =='' || responseData.customer_shopify_id ==null || responseData.customer_stripe_id ==''  ||  responseData.customer_stripe_id ==null) &&  responseData.status != 'active' ){
                          var shopifyID = responseData.customer_shopify_id;
                          var stripeID = responseData.customer_stripe_id;
                          this.cookieService.set( 'fp_waitlist_email',  this.email );
                          this.cookieService.set('fp_waitlist_step',"complete-profile");
                          this.router.navigate(['complete-profile']);
                      //    this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                    }
                    else if((responseData.selected_plan !=''  && responseData.selected_plan !=null && responseData.selected_plan.length >0) ||  responseData.status == 'active' ){
                          this.cookieService.set('fp_waitlist_email',  this.email);
                          this.cookieService.set('CustomerStripeID',  btoa(responseData.customer_stripe_id));
                          this.cookieService.set('CustomerShopifyID', btoa(responseData.customer_shopify_id));  
                          this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                          this.cookieService.set('fp_waitlist_selected_plan',responseData.selected_plan);
                          this.cookieService.set('fp_waitlist_step',"priority-list");
                          this.router.navigate(['priority-list']);
                          /*window.location.href =  this.data.SHOPIFY_FRONTEND_URL+"?openlogin="+btoa(this.email);*/
                    }else{
                          this.cookieService.set('fp_waitlist_email',  this.email);
                          this.cookieService.set('CustomerStripeID',  btoa(responseData.customer_stripe_id));
                          this.cookieService.set('CustomerShopifyID', btoa(responseData.customer_shopify_id));  
                          this.cookieService.set('fp_waitlist_fullname', responseData.firstname+" "+responseData.lastname);
                          if(responseData.oldCustomer){
                              this.cookieService.set('fp_waitlist_oldCustomer', "true");
                          }

                          this.data.getCustomerStripeCard().subscribe(
                            data  => {
                                if(data){
                                  var previousCard  = "XXXX XXXX XXXX "+data[0];
                                  if(this.cookieService.get('fp_waitlist_previousCard')){
                                      this.cookieService.delete('fp_waitlist_previousCard');
                                  }
                                  this.cookieService.set('fp_waitlist_previousCard', previousCard+","+data[1]+","+data[2]+","+data[3]);
                                }else{}
                            },
                            error => {
                                console.log("Card Error", error);
                            });

                          var logedInUser = "false";
                          if(this.cookieService.get('fp_waitlist_loggedin_customer')){
                              logedInUser = "true";
                          }

                          if(this.cookieService.get('fp_waitlist_selected_plan')){ //&&  logedInUser == "true"
                            this.cookieService.set('fp_waitlist_step',"purchase-plan");
                            this.router.navigate(['purchase-plan']);
                          }
                          /*else if(this.cookieService.get('fp_waitlist_step')){

                          }*/
                          else{
                                if(this.cookieService.get('fp_waitlist_oldCustomer')){
                                    this.cookieService.set('fp_waitlist_step',"thankyou");
                                    this.router.navigate(['thankyou']);
                                }else{
                                    this.cookieService.set('fp_waitlist_step',"plans");
                                    this.router.navigate(['plans']);
                                }
                          }
                    }
                  //console.log();
             }
             this.spinner.hide();
        },
        error => {
            console.log("Error", error);
        }
    );
    
  }

} 
function firstLetterCheck(control: FormControl) {
  let data = control.value;
  if(data.length > 0){
    var firstLetter = data.substr(0,1);
    console.log(firstLetter);
    if(firstLetter == "." || firstLetter == "_" || firstLetter == "-"){
      return {
        CheckFirstLetter:{
          chk:true
        }
      }
    }
    return null;
  }
  return null;
}
