import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WaitlistComponent } from './waitlist/waitlist.component';
import { CompleteProfileComponent } from './complete-profile/complete-profile.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { PlansComponent } from './plans/plans.component';
import { PurchasePlanComponent } from './purchase-plan/purchase-plan.component';
import { PriorityListComponent } from './priority-list/priority-list.component';
import { NewWaitlistComponent } from './new-waitlist/new-waitlist.component';

const routes: Routes = [
    { path:'', redirectTo:'/waitlist', pathMatch:'full'},
    { path:"old-waitlist",  component:WaitlistComponent },
    { path:"complete-profile",  component:CompleteProfileComponent }, 
    { path:"thankyou",  component:ThankyouComponent },
    { path:"purchase-plan",  component:PurchasePlanComponent },
    { path:"plans",  component:PlansComponent },
    { path:"waitlist",  component:NewWaitlistComponent },
    { path:"priority-list",  component:PriorityListComponent }
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
