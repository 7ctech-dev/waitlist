import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import {  CountdownComponent  } from 'ngx-countdown';
import { NgxSpinnerService } from 'ngx-spinner';
import '../../assets/fb_pixels.js';
declare var setFBPixel: any;
@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.css']
})
export class PlansComponent implements OnInit {
  @ViewChild('countdown') counter: CountdownComponent;
  waitTime : string;
  waitTimeHours:number;
  constructor(
    private data:DataService, 
    private router: Router,
    private spinner: NgxSpinnerService,
    private cookieService: CookieService) { }

  SocialitePlanPrice    : string;
  TrendsetterPlanPrice  : string;
  WanderlustPlanPrice   : string;
  check30Credit  : any;
  counterLeftTime: any;
  lastOrderCheck:any;
  ApplyBlackFriday:any;
  ApplyPauseCustomerView:any;
  plansSpotsLeft:any
  ngOnInit() { 
    if(this.cookieService.get('fp_waitlist_selected_plan')){
      this.router.navigate(['purchase-plan']);
    }
    var newTime = 300;
    this.lastOrderCheck = 'false';
    this.ApplyPauseCustomerView = 'false';
    if(this.cookieService.get("countdown_timer")){
        var countdown_timer = this.cookieService.get("countdown_timer");
        var split_timer = countdown_timer.split(':');
        newTime =  (parseInt(split_timer[0])*60)+parseInt(split_timer[1]);
        this.cookieService.delete("countdown_timer");
    }
    if(this.cookieService.get('fp_waitlist_last_order')){
        this.lastOrderCheck = 'true';
    }

    if(this.cookieService.get('black_sale_pause_customer') || this.cookieService.get('fp_waitlist_oldCustomer')){
        this.ApplyPauseCustomerView = 'false';  
    }

    this.counterLeftTime = newTime;
    this.SocialitePlanPrice   = this.data.SocialitePlanPrice;
    this.TrendsetterPlanPrice = this.data.TrendsetterPlanPrice;
    this.WanderlustPlanPrice  = this.data.WanderlustPlanPrice;
    this.plansSpotsLeft = {socialite:4,trendsetter:1,wanderlust:2}
    this.data.checkPageView();
    new setFBPixel();

    
    //BLACK FRIDAY
    if(this.data.BLACK_FRIDAY_SALE == true){
      this.ApplyBlackFriday = true;
    }else{
      this.ApplyBlackFriday = false;
    }
    // var postUrlBanjo = "interactions/getWaitlistTime";              
    this.data.getWaitlistTIme()
     .subscribe(data => {
       if(data.indexOf('HRS') > -1){
          this.waitTimeHours = parseInt(data.split(' ')[0])
       }else{
        this.waitTimeHours = 0;
       }
       this.waitTime = data;
       this.cookieService.set( 'wt_hour',  data);
     })
    // this.waitTime = "72 HRS";

      this.data.check30CreditOn().subscribe(data => {
        console.log(data +" check $30 credit ====-------");
        this.cookieService.set('fp_waitlist_check30CreditOn', data);
        this.check30Credit = data;
       })
       if(this.cookieService.get("plansSpotsLeft")){
        this.plansSpotsLeft = JSON.parse(this.cookieService.get("plansSpotsLeft"));
        console.log(this.plansSpotsLeft)
        // this.PurchasePlanForm.controls['ref_code'].disable();
      }
  }
  showFinishMessage(){
    this.counterLeftTime = 300;
    setTimeout(() => this.counter.restart());
  }

  purchasePlane(planName, planAmount){ 
          let countdown = document.getElementsByTagName('countdown')[0];
          this.cookieService.set('countdown_timer', countdown.textContent);
          this.cookieService.set( 'fp_waitlist_selected_plan',  planName );
          this.cookieService.set( 'fp_waitlist_step',  "purchase-plan" );
          if(this.cookieService.get('fp_waitlist_email_type')){
            var obj = {"email":this.cookieService.get('fp_waitlist_email'),"email_type":this.cookieService.get('fp_waitlist_email_type'),"event":'click_select_plan'};
            console.log(obj);
            this.data.setUserActivityBanjo(obj).subscribe(data => {
              console.log('data responce'+data)
            });
          }
          this.router.navigate(['purchase-plan']);
        
  }
  
}
