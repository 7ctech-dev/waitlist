import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxSpinnerModule } from 'ngx-spinner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WaitlistComponent } from './waitlist/waitlist.component';
import { CompleteProfileComponent } from './complete-profile/complete-profile.component';
import { ThankyouComponent } from './thankyou/thankyou.component';
import { PlansComponent } from './plans/plans.component';
import { CountdownModule } from 'ngx-countdown';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {NgxMaskModule} from 'ngx-mask'
import { CookieService } from 'ngx-cookie-service';
import { DataService } from './data.service';
import { PurchasePlanComponent } from './purchase-plan/purchase-plan.component';
import { PriorityListComponent } from './priority-list/priority-list.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NewWaitlistComponent } from './new-waitlist/new-waitlist.component';

@NgModule({
  declarations: [
    AppComponent,
    WaitlistComponent,
    CompleteProfileComponent,
    ThankyouComponent,
    PlansComponent,
    PurchasePlanComponent,
    PriorityListComponent,
    HeaderComponent,
    FooterComponent,
    NewWaitlistComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NgxSpinnerModule,
    CountdownModule,
    NgxMaskModule.forRoot()
  ],
  providers: [DataService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
