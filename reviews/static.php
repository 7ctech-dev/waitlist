<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="initial-scale=1, maximum-scale=1">
<title>Google Reviews</title>
<link rel="stylesheet" href="assets/css/bootstrap.min.css">
<link rel="stylesheet" href="assets/css/font-awesome.min.css">
<script type="text/javascript" src="assets/js/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<style>
    @import url('https://fonts.googleapis.com/css?family=Open+Sans');
    @import url('https://fonts.googleapis.com/css?family=Open+Sans:700');
    ::-moz-selection { /* Code for Firefox */
    color: white;
    background: #FF8A95;
    }
    .checked {
        color: #fc9297;
    }

    span.fa.fa-star.checked {
        font-size: 2.3em;
    }

    .rwstars .fa.fa-star.checked {
        font-size: 1.5em !important;
    }

    .multi-item-carousel .carousel-inner>.item {
        transition: 500ms ease-in-out left;
    }

    .multi-item-carousel .carousel-inner .active.left {
        left: -50%;
    }

    .multi-item-carousel .carousel-inner .active.right {
        left: 50%;
    }

    .multi-item-carousel .carousel-inner .next {
        left: 50%;
    }

    .multi-item-carousel .carousel-inner .prev {
        left: -50%;
    }

    @media all and (transform-3d),
    (-webkit-transform-3d) {
        .multi-item-carousel .carousel-inner>.item {
            transition: 500ms ease-in-out left;
            transition: 500ms ease-in-out all;
            -webkit-backface-visibility: visible;
            backface-visibility: visible;
            -webkit-transform: none !important;
            transform: none !important;
        }
    }

    .multi-item-carousel .carouse-control.left,
    .multi-item-carousel .carouse-control.right {
        background-image: none;
    }

    #theCarousel {
        margin-left: 80px;
        margin-right: 80px;
    }

    .carousel-control.left {
        margin-left: -10%;
        background: none;
        color: #fc9297;
    }

    .carousel-control.right {
        margin-right: -10%;
        background: none;
        color: #fc9297;
    }

    i.glyphicon.glyphicon-chevron-left {
        font-size: 2em;
        font-weight: normal;
        -webkit-text-stroke: 5px white;
        text-shadow: none;
    }

    i.glyphicon.glyphicon-chevron-right {
        font-size: 2em;
        font-weight: normal;
        -webkit-text-stroke: 5px white;
        text-shadow: none;
    }

    .rwtext {
        background-color: #fae3e3;
        margin: 0 2em;
        padding: 1.2em 2em;
        font-family: 'Open Sans';
        float: left;
        min-height: 9em;
        display: flex;
        align-items: center;
        min-width:88%;
    }

    .rwtext p {
        font-size: 1.2em;
        font-weight: 500;
        color: #5d5d5d;
        line-height: 1.3em;
    }
    .rwtext p a {
        color: #5d5d5d;
        text-decoration: underline;
    }

    .rwinfo {
        margin: 0.5em 3.7em;
        float: left;
        clear: both;
    }

    .trishape {
        width: 0;
        height: 0;
        border-top: 30px solid #fae3e3;
        border-left: 30px solid transparent;
        float: left;
        margin: 0;
        padding: 0;
        left: 3.5em;
        position: relative;
    }

    .rwinfo img {
        float: left;
        border-radius: 50px;
    }

    span.rwname {
        padding: 0.5em 0 0 1em;
        display: inline;
        white-space: nowrap;
        font-family: 'Open Sans';
        font-size: 1.2em;
        font-weight: 500;
    }

    span.rwstars {
        padding: 0.5em 0 0 1em;
        display: inline;
        white-space: nowrap;
    }

    span.rwtime {
        display: block;
        padding: 0.5em 7em;
        white-space: nowrap;
        font-family: 'Open Sans';
        font-size: 0.9em;
        font-style: italic;
        position: relative;
        color: #7d7d7d;
    }

    .carousel-indicators li {
        text-indent: 0;
    }

    ol.carousel-indicators {
        position: static;
        margin: 0 auto;
    }

    div#Carousel ol {
        display: none;
    }

    #Carousel .row {
        display: flex;
        align-items: center;
    }

    #Carousel i.glyphicon.glyphicon-chevron-right {
        top: 42%;
        margin-right: -13%;
    }

    #Carousel i.glyphicon.glyphicon-chevron-left {
        top: 42%;
        margin-left: -18%;
    }

    .love-css {
        -webkit-box-sizing: content-box;
        -moz-box-sizing: content-box;
        font: 700 3.9em "Open Sans";
        color: #A1D5D9;
        text-align: center;
        -o-text-overflow: ellipsis;
        text-overflow: ellipsis;
        text-shadow: -3px -2px 1px #A1D5D9;
        text-align: center;
        -webkit-text-stroke-width: 0.02em;
        -webkit-text-stroke-color: #565656;
        padding: 0.22em 0 0.45em 0;
        letter-spacing: 1px;
    }

    .love-css .fa.fa-heart {
        font-size: 0.9em;
        color: #fc9297;
    }

    #Carousel a {
        border: none !important;
    }

    #Carousel .col-md-2 {
        padding: 0 0.5em;
    }

    @media screen and (max-width: 767px) {
        .love-css {
            font-size: 2.5em;
        }
        #theCarousel {
			margin-left: 10px;
			margin-right: 10px;
		}
		#Carousel .col-md-2 {
			padding: 0;
			margin: 0;
		}
		#Carousel i.glyphicon.glyphicon-chevron-right {
			top: 30%;
			margin-right: -22px;
		}
		#Carousel i.glyphicon.glyphicon-chevron-left {
			top: 30%;
			margin-left: -22px;
		}
		.container.instasec {
			width: 100% !important;
		}
		.container.grsec {
			width: 100% !important;
		}
    }
    .instasec .row
    {
    margin-right: 0;
    margin-left: 0;
    }
    spna.overallrating {
    font-family: 'Open Sans';
    font-size: 2.5em;
    font-weight: bolder;
    color: #A1D5D9;
    padding: 0 0.4em;
    }
    span.totalrw {
        font-family: 'Open Sans';
    text-decoration: underline;
    font-size: 1.5em;
    padding: 0 1em;
    display: inline-block;
    color: #666;
    }
    #theCarousel i.glyphicon.glyphicon-chevron-left, #theCarousel i.glyphicon.glyphicon-chevron-right {
    top: 20% !important;
}
.ratings {
    position: relative;
    vertical-align: top;
    display: inline-block;
    color: #b1b1b1;
    overflow: hidden;
    margin-top: -5px;
}

.full-stars{
  position: absolute;
  left: 0;
  top: 0;
  white-space: nowrap;
  overflow: hidden;
  color: #fde16d;
}

.empty-stars:before,
.full-stars:before {
  content: "\2605\2605\2605\2605\2605";
  font-size: 3em;
}

.empty-stars:before {
  -webkit-text-stroke: 1px #848484;
}

.full-stars:before {
  -webkit-text-stroke: 1px #F69295;
}

/* Webkit-text-stroke is not supported on firefox or IE */
/* Firefox */
@-moz-document url-prefix() {
  .full-stars{
    color: #F69295;
  }
}

  .full-stars{
    color: #F69295;
  }
</style>
</head>
<body>
<div class="love-css">WHY MEMBERS
  <p class="fa fa-heart"></p>
  US!</div>
<div class="container instasec">
  <div class="row">
    <div class="col-md-12">
      <div id="Carousel" class="carousel slide">
        <!-- Carousel items -->
        <div class="carousel-inner" id="includeInstagramPostHtml">
		<?php include("instagramPosts.html"); ?>
			  </div>
			  
        <!--.carousel-inner-->
        <a class="left carousel-control" href="#Carousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a> <a class="right carousel-control" href="#Carousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a> </div>
      <!--.Carousel-->
    </div>
  </div>
</div>
<div style="width: 100%; margin: 0 auto; text-align: center; padding: 2em 0 3em 0;">
  <spna class="overallrating">5.0</spna>
  <div class="ratings">
    <div class="empty-stars"></div>
    <div class="full-stars" style="width:90%"></div>
  </div> <span class="totalrw">230 Google Reviews</span> </div>
<div class="container grsec">
  <div class="row">
    <div class="col-md-12">
      <div class="carousel slide multi-item-carousel grc" id="theCarousel">

            <div class="carousel-inner gr" id="includeReviewSectionHTMl">
			<?php include("reviewSection.html"); ?>
				</div>
				
        <a class="left carousel-control" href="#theCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a> <a class="right carousel-control" href="#theCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a> </div>
    </div>
  </div>
</div>
</body>
<script>
 $(document).ready(function () {
    $('#theCarousel').carousel({
        interval: 8000
    })

    $('.grc .item').each(function () {
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));


    });
    var slider = $(".grc");

    slider.append("<ol class='carousel-indicators abc'></ol>");  //prepend instead if you want it at the beginning

    var sliderInd = $(".carousel-indicators");

    slider.find(".gr").children(".item").each(function (index) {
        (index === 0) ?
            sliderInd.append("<li data-target='#theCarousel' data-slide-to='" + index + "' class='active'>" + (index + 1) + "</li>") :
            sliderInd.append("<li data-target='#theCarousel' data-slide-to='" + index + "'>" + (index + 1) + "</li>");
    });

    
        $('#Carousel').carousel({
            interval: 5000
        })
   
	})
   
</script>
</html>
