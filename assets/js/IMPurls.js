// var STRIPE_PROXY_URL   =  "https://hautedb.com/hautapi/api/";
// var SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api/";
// var DBINFOURL          =  "https://hautedb.com";

STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_1/"; 
SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_1/";
DBINFOURL        = "https://stageone.hautedb.com";

if(window.location.href.indexOf('haute-and-borrowed-stage-2') != -1){
    STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_2/";
    SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_2/";
    DBINFOURL        = "https://stagetwo.hautedb.com";
}
else if(window.location.href.indexOf('haute-and-borrowed-stage-3') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_3/";
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_3/";
  DBINFOURL        = "https://stagethree.hautedb.com";
}
else if(window.location.href.indexOf('haute-and-borrowed-stage-4') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_4/";
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_4/";
  DBINFOURL        = "https://stagefour.hautedb.com";
}
else if(window.location.href.indexOf('haute-and-borrowed-stage-5') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_5/"; 
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_5/";
  DBINFOURL        = "https://stagefive.hautedb.com";
}else if(window.location.href.indexOf('haute-and-borrowed-stage-7') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_7/"; 
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_7/";
  DBINFOURL        = "https://stageseven.hautedb.com";
}
else if(window.location.href.indexOf('haute-and-borrowed-stage-1') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_1/"; 
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_1/";
  DBINFOURL        = "https://stageone.hautedb.com";
}else if(window.location.href.indexOf('haute-and-borrowed-stage-main') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api_stage_main/"; 
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api_stage_main/";
  DBINFOURL        = "https://stagemain.hautedb.com";
}
 
else if(window.location.href.indexOf('haute-and-borrowed-stage') != -1){
  STRIPE_PROXY_URL = "https://hautedb.com/hautapi/api/";
  SHOPIFY_PROXY_URL = "https://hautedb.com/hautapi/api/";
  DBINFOURL        = "https://stage.hautedb.com";
}
//Following two are for LIVE:
else if(window.location.href.indexOf('haute-and-borrowed.') != -1){
  STRIPE_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  SHOPIFY_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  DBINFOURL          =  "https://data.thefashionpass.com";
}
else if(window.location.href.indexOf('hauteandborrowed.com') != -1){
  STRIPE_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  SHOPIFY_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  DBINFOURL          =  "https://data.thefashionpass.com";
}
else if(window.location.href.indexOf('thefashionpass.com') != -1){
  STRIPE_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  SHOPIFY_PROXY_URL = "https://data.thefashionpass.com/router/api_live/";
  DBINFOURL          =  "https://data.thefashionpass.com";
}