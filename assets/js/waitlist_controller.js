var IP_ADDRESS = 0;

$.getJSON("https://api.ipify.org/?format=json", function(e) {
  IP_ADDRESS = e.ip;
  console.log("CUST_IP:"+IP_ADDRESS);
  if(e.ip == "68.6.157.38"){//68.6.157.38
    alert("Something went wrong. Please try latter.");
    window.badcust = true;
    window.location = STORE_URL + "/account/logout";
  }
});



//Danish Code Starts//
// $('.nav-tabs > li:first-child > a')[0].click();
$(document).ready(function() {
  $('#carousel').carousel({
    interval: 10000
})
});
$(document).ready(function() {
  $('#carousel2').carousel({
    interval: 10000
})
});
$(document).ready(function() {
  $('#carousel3').carousel({
    interval: 10000
})
});
$().ready(function() {
          $('#planname1').arctext({radius: 1500});
});
$().ready(function() {
    $('#planname2').arctext({radius: 2000});
});
$().ready(function() {
    $('#planname3').arctext({radius: 2000});
});

// +++++MODAL SCRIPT+++++
// var currentTab = 0; // Current tab is set to be the first tab (0)
// showTab(currentTab); // Display the crurrent tab

// function showTab(n) {
//   // This function will display the specified tab of the form...
//   var x = document.getElementsByClassName("tab");
//   x[n].style.display = "block";
//   //... and fix the Previous/Next buttons:
//   if (n == 0) {
//     document.getElementById("prevBtn").style.display = "none";
//   } else {
//     document.getElementById("prevBtn").style.display = "inline";
//   }
//   //... and run a function that will display the correct step indicator:
//   fixStepIndicator(n)
// }

// function nextPrev(n) {
//   // This function will figure out which tab to display
//   var x = document.getElementsByClassName("tab");
//   // Exit the function if any field in the current tab is invalid:
//   if (n == 1 && !validateForm()) return false;
//   // Hide the current tab:
//   x[currentTab].style.display = "none";
//   // Increase or decrease the current tab by 1:
//   currentTab = currentTab + n;
//   // if you have reached the end of the form...
//   if (currentTab >= x.length) {
//     // ... the form gets submitted:
//     document.getElementById("regForm").submit();
//     return false;
//   }
//   // Otherwise, display the correct tab:
//   showTab(currentTab);
// }

// function validateForm() {
// // This function deals with validation of the form fields
//   var x, y, i, valid = true;
//   x = document.getElementsByClassName("tab");
//   y = x[currentTab].getElementsByTagName("input");
//   // A loop that checks every input field in the current tab:
//   for (i = 0; i < y.length; i++) {
//     // If a field is empty...
//     if (y[i].value == "") {
//       // add an "invalid" class to the field:
//       y[i].className += " invalid";
//       // and set the current valid status to false
//       valid = false;
//     }
//   }
//   // If the valid status is true, mark the step as finished and valid:
//   if (valid) {
//     document.getElementsByClassName("step")[currentTab].className += " finish";
//   }
//   return valid; // return the valid status
// }

// function fixStepIndicator(n) {
//   // This function removes the "active" class of all steps...
//   var i, x = document.getElementsByClassName("step");
//   for (i = 0; i < x.length; i++) {
//     x[i].className = x[i].className.replace(" active", "");
//   }
//   //... and adds the "active" class on the current step:
//   x[n].className += " active";
// }
//Danish Code Ends//

//Ahmed Code Starts//
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

function daysInThisMonth(year,month) {
  var now = new Date();
  return new Date(year,month, 0).getDate();
}



function resettimer_step3(){
  clearInterval(window.interval_step_3);
  var timer2 = "5:00";
  window.interval_step_3 = setInterval(function() {
    var timer = timer2.split(':');
    //by parsing integer, I avoid all extra string processing
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    --seconds;
    minutes = (seconds < 0) ? --minutes : minutes;
    seconds = (seconds < 0) ? 59 : seconds;
    seconds = (seconds < 10) ? '0' + seconds : seconds;
    //minutes = (minutes < 10) ?  minutes : minutes;
    $('#priority_timer_step_3').html(minutes + ':' + seconds);
    if (minutes < 0) clearInterval(window.interval_step_3);
    //check if both minutes and seconds are 0
    if ((seconds <= 0) && (minutes <= 0))
    {
      clearInterval(window.interval_step_3);
      $('#requestform').modal('hide');
    } 
    timer2 = minutes + ':' + seconds;
  }, 1000);
}

function resettimer_step4(){
  clearInterval(window.interval_step_4);
  var timer2 = "5:00";
  window.interval_step_4 = setInterval(function() {
    var timer = timer2.split(':');
    //by parsing integer, I avoid all extra string processing
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    --seconds;
    minutes = (seconds < 0) ? --minutes : minutes;
    seconds = (seconds < 0) ? 59 : seconds;
    seconds = (seconds < 10) ? '0' + seconds : seconds;
    //minutes = (minutes < 10) ?  minutes : minutes;
    $('#priority_timer_step_4').html(minutes + ':' + seconds);
    if (minutes < 0) clearInterval(window.interval_step_4);
    //check if both minutes and seconds are 0
    if ((seconds <= 0) && (minutes <= 0))
    {
      clearInterval(window.interval_step_4);
      $('#requestform').modal('hide');
    } 
    timer2 = minutes + ':' + seconds;
  }, 1000);
}

function resettimer_step5(){
  clearInterval(window.interval_step_5);
  var timer2 = "5:00";
  window.interval_step_5 = setInterval(function() {
    var timer = timer2.split(':');
    //by parsing integer, I avoid all extra string processing
    var minutes = parseInt(timer[0], 10);
    var seconds = parseInt(timer[1], 10);
    --seconds;
    minutes = (seconds < 0) ? --minutes : minutes;
    seconds = (seconds < 0) ? 59 : seconds;
    seconds = (seconds < 10) ? '0' + seconds : seconds;
    //minutes = (minutes < 10) ?  minutes : minutes;
    $('#priority_timer_step_5').html(minutes + ':' + seconds);
    if (minutes < 0) clearInterval(window.interval_step_5);
    //check if both minutes and seconds are 0
    if ((seconds <= 0) && (minutes <= 0))
    {
      clearInterval(window.interval_step_5);
      $('#requestform').modal('hide');
    } 
    timer2 = minutes + ':' + seconds;
  }, 1000);
}

function numberonly(event, space) {
  event.preventDefault();
  event.preventDefault();
  starte = $(event.target).get(0).selectionStart
  ende = $(event.target).get(0).selectionEnd
  data = event.data;
  if(data ==  null){
    if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
    return ;
  }
  for(i=0;i<data.length;i++){
    kp = data[i];
    var key = kp.charCodeAt(0);
    if((key >= 48 && key <= 57 || key == 8 || key == 32)) {
      var curspos =  $(event.target).val().indexOf(kp);
      if(curspos-1 > 0){
        debugger;
        if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
      }
    }
    else {
      var curspos =  $(event.target).val().indexOf(kp);
      if(curspos-1 > 0){
        if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
      }
      $(event.target).val($(event.target).val().replace(kp,''));
      if(curspos-1 > 0){
        if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
      }
    }
  }
}

function alphaOnly(event) {
  event.preventDefault();
  event.preventDefault();
  data = event.data;
  starte = $(event.target).get(0).selectionStart
  ende = $(event.target).get(0).selectionEnd
    val = $(event.target).val();

    if(/\s/g.test(val.charAt(0))){
      $(event.target).val("");
    }

  


  if(data ==  null){
    if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
    return false;
  }
  for(i=0;i<data.length;i++){
    kp = data[i];
    var key = kp.charCodeAt(0);
    if((key >= 65 && key <= 90) || (key >= 95 && key <= 122) || key == 8 || key == 32) {
      var curspos =  $(event.target).val().indexOf(kp);
      if(curspos-1 > 0){
        if(window.width<600){
          setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);
        }
        else{
          //setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);
        }
      }
      return false;
    }
    else {
      var curspos =  $(event.target).val().indexOf(kp);
      if(curspos-1 > 0){
        if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
      }
      $(event.target).val($(event.target).val().replace(kp,''));
      if(curspos-1 > 0){
        if(window.width<600){           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},80);         }         else{           setTimeout(function(){$(event.target).get(0).setSelectionRange(starte,ende);},90);         }
      }
    }
  }
}

function handlewaitlistcheckout()
{
  
  debugger;
  var wt_agree_check = $('#agree_check_step_5').is(":checked");
  removeborderred();
  if(wt_agree_check ==  true)
  {
    debugger;
    var wt_regex_zip = new RegExp('^[0-9]{5}$');
    var wt_regex_cvc = new RegExp('^[0-9]{3,4}$');
    var wt_regex_card_name = new RegExp('^([a-zA-Z ]){2,100}$');
    var wt_regex_card_number = new RegExp('^[0-9]{13,16}$');

    var wt_nameOnCard =        $('#card_full_name_step_5').val();
    var wt_CCNumber =          $('#ccard_number_step_5').val(); 
    var wt_cardExpirMonthYear =    $('#card_expire_month_year_step_5').val();
    var wt_cardCVC =           $('#card_cvc_step_5').val();
    var wt_zip =        $('#zip_code_step_5').val();
    var valid = true;

    if(wt_nameOnCard == "" || !wt_regex_card_name.test(wt_nameOnCard)){
      borderRed('card_full_name_step_5');
      valid = false;
    }
    if(wt_CCNumber == "" || !wt_regex_card_number.test(wt_CCNumber)){
      borderRed('ccard_number_step_5');
      valid = false;
    }
    if(wt_cardCVC == "" || !wt_regex_cvc.test(wt_cardCVC)){
      borderRed('card_cvc_step_5');
      valid = false;
    }
    if(wt_zip == "" || !wt_regex_zip.test(wt_zip)){
      borderRed('zip_code_step_5');
      valid = false;
    }

    if(wt_cardExpirMonthYear == ""){
      borderRed('card_expire_month_year_step_5');
      valid = false;
    }

    if(valid)
    {
      var wt_cardLast4 = '';
      wt_cardLast4 = $('#ccard_number_step_5:visible').val().slice(-4);
      var wt_expirymonthyeararr = wt_cardExpirMonthYear.split('/');
      var wt_expiry_month = wt_expirymonthyeararr[0];
      var wt_expiry_year = wt_expirymonthyeararr[1];

      var currentDT = new Date();
      yearstored =parseInt(wt_expiry_year);
      dtyear = currentDT.getFullYear();
      if(yearstored <= dtyear ){
        if(parseInt(wt_expiry_month) <= currentDT.getMonth() || parseInt(wt_expiry_month) > 12 ){
            borderRed('card_expire_month_year_step_5');
            valid = false;
        }

        if( wt_CCNumber != ""){
            var cardExpirYears = parseInt(wt_expiry_year);
            var cardExpirMonths = (parseInt(wt_expiry_month)-1);
            var cardExpireDates = new Date(cardExpirYears, cardExpirMonths);

            Date.prototype.addMonths = function (m) {
                var d = new Date(this);
                var years = Math.floor(m / 12);
                var months = m - (years * 12);
                if (years) d.setFullYear(d.getFullYear() + years);
                if (months) d.setMonth(d.getMonth() + months);
                return d;
            }

            var compareExpireDates = new Date().addMonths(1);
            if (cardExpireDates.getTime() <= compareExpireDates.getTime()) {
              cardExpireValidation('card_expire_month_year_step_5');
              valid = false;
            }
        }
      }
    }

    if(!valid)
    {
      $('#validation_alert_step_5').css({"color":"red"});
      $('#validation_alert_div_step_5').show();
      $('#loading_step_5').hide();
      $('#wt_submit_btn_step_5').show();
      return false;
     
    }

    var ShopifyCustomer = GetCustomer($.cookie("waitlist_customer_email"));
    window.ShopifyCustomer = ShopifyCustomer;
    var StripeCustomer = false;

    if (ShopifyCustomer.count ) {
        StripeCustomer = retrieve_a_customer_Stripe(ShopifyCustomer.data[0].id);
    }
    if (StripeCustomer != false && StripeCustomer.object == "customer" ) {
        ////customer is already in stripe! 
        debugger;
        addNewCardWithalldetails();
    }
  }
  else
  {
    debugger;
    $('#loading_step_5').hide();
    $('#wt_submit_btn_step_5').show();
    $('#validation_alert_div_step_5').show();
    $('#validation_alert_step_5').css({"color":"red"});
    $('#validation_alert_step_5').text("Please check the agreement first");
    return false;
  }
}

function borderRed(item_id){
  $('#'+item_id).addClass('Redboreder');
  $('.validation_alert_step_5').text('Please enter proper values in RED marked fields and check all the options');
}

function cardExpireValidation(item_id){
  $('#'+item_id).addClass('Redboreder');
  $('.validation_alert_step_5').text('Please enter a card that does not expire in the next month.');
 }


 function addNewCardWithalldetails(){
  debugger;
  loadPoints($.cookie("waitlist_customer_email"),false);
  
  var wt_nameOnCard =        $('#card_full_name_step_5').val();
  var wt_CCNumber =          $('#ccard_number_step_5').val();
  var wt_cardLast4 = $('#ccard_number_step_5:visible').val().slice(-4);
  var wt_cardExpirMonthYear =    $('#card_expire_month_year_step_5').val();
  var wt_expirymonthyeararr = wt_cardExpirMonthYear.split('/');
  var wt_expiry_month = wt_expirymonthyeararr[0];
  var wt_expiry_year = wt_expirymonthyeararr[1];
  
  var wt_cardCVC =           $('#card_cvc_step_5').val();
  var wt_zip =               $('#zip_code_step_5').val();
  

  ////problem starts from here !!!!
  debugger;
  var stripevalidation = true;
  card_data={
      "url": "/customers/" +window.ShopifyCustomer.data[0].id + "/sources",
      "method": "POST",
      "headers": {
          "authorization": "Bearer "
      },
      "data": {
          "source": {
              "object" : "card",
              "exp_month" : wt_expiry_month,
              "exp_year" : wt_expiry_year,
              "number" : wt_CCNumber,
              "currency" : "usd",
              "cvc" : wt_cardCVC,
              "name" : wt_nameOnCard,
              "address_country" : "US",
              "address_zip"   : wt_zip,
              "default_for_currency" : "false"
          }
      }
  };
  $.ajax({
      async: false,
      url: STRIPE_PROXY_URL + "generic_stripe_request.php",
      data: JSON.stringify(card_data),
      dataType: "json",
      method: "POST"
  }).done(function (new_card) {
      if(new_card.error != undefined){
          if(new_card.error.decline_code == "fraudulent"){
              $('.validation_alert_step_5').text("Your card was declined because it is linked to a history of fraudulent activity. This may be because you have reported fraudulent activity on your card in the past. Please choose a different card.");
              $('#validation_alert_step_5').css({"color":"red"});
              $('#loading_step_5').hide();
              $('#wt_submit_btn_step_5').show();
              $('#validation_alert_div_step_5').show();
              return false;
          }
          else if(new_card.error.code == "incorrect_cvc" || new_card.error.code == "card_declined" || new_card.error.code == "expired_card" || new_card.error.code == "incorrect_number") { 
              $('.validation_alert_step_5').text(new_card.error.message);
              $('#validation_alert_step_5').css({"color":"red"});
              $('#loading_step_5').hide();
              $('#wt_submit_btn_step_5').show();
              $('#validation_alert_div_step_5').show();
              return false;
            }
          else{
            $('.validation_alert_step_5').text(new_card.error.message);
            $('#validation_alert_step_5').css({"color":"red"});
            $('#loading_step_5').hide();
            $('#wt_submit_btn_step_5').show();
            $('#validation_alert_div_step_5').show();
            return false;
          }
      }
      else{
        debugger;
          makeThisCardDef(window.ShopifyCustomer.data[0].id,new_card.id,false);
          debugger;
          if(window.defDone)
          {
            debugger;
            $.ajax({
              url: DBINFOURL + '/Waitlistcontroller/update_waitlist_customer',
              data: {
                'customer_stripe_id' : window.ShopifyCustomer.data[0].id,
                'customer_card_last4' : wt_cardLast4,
                'selected_plan' : $.cookie('wt_selected_plan'),
                'plan_value' : $.cookie("wt_selected_plan_price")
              },
              dataType: "json",
              method: "POST",
            }).done(function (response) {
              debugger;
              if(response)
              {
                $('#validation_alert_div_step_5').hide();
                $('#validation_alert_step_5').text("");
                $("#wt_popup_step_5").hide();
                $.cookie("wt_previous_step", '0' );
                $('#validation_alert_step_6').text("");
                $('#wt_popup_step_6').show();
                $('#validation_alert_div_step_5').hide();
                $('#validation_alert_step_5').text("");
                $('#loading_step_5').hide();
                $('#wt_submit_btn_step_5').show();
              }
              
            })
             
          }

          return true;
      }
  });
}

function makeThisCardDef(customerId , cardId,hold){
  window.defDone = false;
  if(hold == undefined){
      hold= true;
  }
 var settings = {
     "url": "/customers/" + customerId,
     "method": "POST",
     "headers": {
         "authorization": "Bearer "
     },
     "data": {
         "default_source": cardId,
     }
 };

 $.ajax({
     async: hold,
     url: SHOPIFY_PROXY_URL + "generic_stripe_request.php",
     data: JSON.stringify(settings),
     dataType: "json",
     method: "POST"
     }).done(function (response) {
         window.defDone = true;


 });
}

function loadPoints(email,async){
  if(async == undefined){
      async = true;
  }
  $.ajax({
    "async": async,
    "url": DBINFOURL + "/Customer_data_endpoint/loadPoints?email="+email
  }).done(function(response){
     window.CustomerPoints = response;
     //window.CustomerPoints = 30;
     if(window.CustomerPoints > 0){
         $('#fpcradit').parents('.billing_section').show();
         $('#fpcradit_mob').parents('.billing_section').show();
         var customerNewPoints = window.CustomerPoints;
          if($('.tag').text().indexOf('30credit') > -1){
              //30credit work to 40 
             customerNewPoints =  (parseFloat(customerNewPoints)+40)
          }
         $('.fpcradit').text(customerNewPoints); 
         $('.fpcradits').text(parseFloat(customerNewPoints));
          $(".friendrefferlink").hover(function(){
                  $(".memeberRewardTooltip").show();
              },function(){
                  $(".memeberRewardTooltip").hide();
          });
         $('.fpcraditmenu').html('(<span style="color:#ff9bda;font-weight:bold;">$'+parseFloat(customerNewPoints)+'</span>)');
         $('.focraditdiv').show();
         if(window.location.href.indexOf('view=checkout&plan_id') != -1){
             populatecredit();
         }
     }else if($('.tag').text().indexOf('30credit') > -1){

          var customerCreditPoints = 40;
          
          $('.fpcradit').text(customerCreditPoints); 
          $('.fpcradits').text(customerCreditPoints);
          $(".friendrefferlink").hover(function(){
                  $(".memeberRewardTooltip").show();
              },function(){
                  $(".memeberRewardTooltip").hide();
          });
          $('.fpcraditmenu').html('(<span style="color:#ff9bda;font-weight:bold;">$'+customerCreditPoints+'</span>)');
     }
  });
}

function camelize(str) {
  //chr => chr.toUpperCase()
      return str.toLowerCase().replace(/\b\w/g, function($1){return $1.toUpperCase()});
  }

function GetCustomer(email) { 
  debugger;
  if(email == undefined)
  {
    var customer = {data:null,count:false};
    customer.data= [];
    return customer;
  }
  window.custcount++
  if(window.custid == undefined || window.custid == "0" || window.emailOfCustomer !=  email ){
    window.emailOfCustomer = email;
    $.ajax({
      "async": false,
      "url": DBINFOURL + "/Customer_data_endpoint/getSidByEmail?email="+window.emailOfCustomer
    }).success(function(response){
      if(response.length < 25){
      window.custid = response.trim();
      
    }else{
        if(window.location.href.indexOf("stage") != -1){
          window.custid = 'cus_BuCWy4hjY24DJW';
        }
    }
    }).error(function(response){
    if(window.location.href.indexOf("stage") != -1){
          window.custid = 'cus_BuCWy4hjY24DJW';
        }
    })
    
  }
  //GetStripeCustomerByID("cus_BHIHwJWRUtMThs")
  var customer = {data:null,count:false};
  customer.data= [];
  cus = GetStripeCustomerByID(window.custid);
  if(cus != null && cus != undefined){
    customer.data[0] = cus;
    customer.count =  1;
  }
  return customer;
}

// Retrieve a customer
function retrieve_a_customer_Stripe(customer_id) {
  var customer = null;

  var settings = {
    "url":  "/customers/" + customer_id,
    "method": "GET",
    "headers": {
      "authorization": "Bearer "
    }
  };
debugger;
  $.ajax({
    async: false,
    url: STRIPE_PROXY_URL + "generic_stripe_request.php",
    data: JSON.stringify(settings),
    dataType: "json",
    method: "POST"
  }).done(function (response) {
    customer = response;
  });

  return customer;
}

function removeborderred()
{
  $('#card_full_name_step_5').removeClass('Redboreder');
  $('#ccard_number_step_5').removeClass('Redboreder');
  $('#card_cvc_step_5').removeClass('Redboreder');
  $('#zip_code_step_5').removeClass('Redboreder');
  $('#card_expire_month_year_step_5').removeClass('Redboreder');
}

// Create a customer
function create_customer_Stripe(data , reloadClear,cname,cbd,bodyt,referral_obj) {
  if(window.customerloadingerror == true ){
    return false;
  }
  if(reloadClear == undefined){
    reloadClear= false
  }
  if(cname == undefined){
    cname= "-1";
  }
  if(cbd == undefined){
    cbd= "-1";
  }
  if(bodyt == undefined){
    bodyt= "-1";
  }
  var customer = null;

  var settings = {
    "url":  "/customers",
    "method": "POST",
    "headers": {
      "authorization": "Bearer "
    },
    "data" : data
  };
  var first_name = cname.split(" ");
  var coupon_id = first_name[0]+"60";
  debugger;
  $.ajax({
    async: false,
    url: STRIPE_PROXY_URL + "generic_stripe_request.php",
    data: JSON.stringify(settings),
    dataType: "json",
    method: "POST"
  }).done(function (response) { 
    window.custid = (response.id).trim();
    customer = response;
    if(referral_obj == undefined || referral_obj == ''){
       $.ajax({
      "async": false,
      "url": DBINFOURL + "/Customer_data_endpoint/stripeDirect?email="+data['email']+"&id="+response.id+"&cname="+cname+"&cbd="+cbd+"&bodyt="+bodyt+"&coupon_id="+coupon_id+"&req=stripeapifunctions132"
    }).done(function(response2){
      console.log('stripedirectresponce');
      console.log(response2);
      response2 = response2.replace(/\s/g, '');
      document.cookie = "user_coupon="+response2;
    });
  }else{
   $.ajax({
      "async": false,
      "url": DBINFOURL + "/Customer_data_endpoint/stripeDirect?email="+data['email']+"&id="+response.id+"&cname="+cname+"&cbd="+cbd+"&bodyt="+bodyt+"&coupon_id="+coupon_id+"&referral_id="+referral_obj.referral_id+"&referral_source="+referral_obj.refferal_source+"&req=stripeapifunctions142"
    }).done(function(response2){
        console.log('stripedirectresponce');
        console.log(response2);
        response2 = response2.replace(/\s/g, '');
        document.cookie = "user_coupon="+response2;
    });
    }
  
  });

  return customer;
}

$(document).ready(function(){

  $('#requestform').on('shown.bs.modal', function (e) {
    debugger;
    if(($.cookie("waitlist_customer_email") != "" && $.cookie("waitlist_customer_email") != null) &&  ($.cookie("wt_previous_step") != '0' && $.cookie("wt_previous_step") != null))
      {
        if($.cookie("wt_previous_step") == '1')
        {
          $('#wt_popup_step_1').show();  
        }
        else if($.cookie("wt_previous_step") == '2')
        {
          $('#wt_popup_step_2').show();  
        }
        else if($.cookie("wt_previous_step") == '3')
        {
          resettimer_step3();
          $('#wt_popup_step_3').show();  
        }
        else if($.cookie("wt_previous_step") == '4')
        {
          resettimer_step4();
          $('#wt_popup_step_4').show();  
        }
        else if($.cookie("wt_previous_step") == '5')
        {
          resettimer_step5();
          $('#wt_popup_step_5').show();  
        }
        else if($.cookie("wt_previous_step") == '6')
        {
          $('#wt_popup_step_6').show();  
        }
      }
      else
      {
        $('#wt_popup_step_1').show();
      }
  });


  $.getScript( "https://cdn.shopify.com/s/files/1/1296/1129/t/2/assets/maskplugin.js", function( data, textStatus, jqxhr ) {
    $('#card_expire_month_year_step_5').mask("99/2099",{placeholder:"MM/YY"});
  });

  $.getScript( "https://cdn.shopify.com/s/files/1/1296/1129/t/2/assets/maskplugin.js", function( data, textStatus, jqxhr ) {
    $('.customer_birthday').mask("99/99/9999",{placeholder:"mm/dd/yyyy"});
  });

  
  $( "#card_full_name_step_5" ).focus(function() {
    resettimer_step5();
  });

  $( "#ccard_number_step_5" ).focus(function() {
    resettimer_step5();
  });

  $( "#card_expire_month_year_step_5" ).focus(function() {
    resettimer_step5();
  });

  $( "#card_cvc_step_5" ).focus(function() {
    resettimer_step5();
  });

  $( "#zip_code_step_5" ).focus(function() {
    resettimer_step5();
  });
  

  

  $(document).on('click', '#wt_submit_btn_step_1' , function(e){
    debugger;
    $.cookie("wt_previous_step", '1' );
    $('#validation_alert_div_step_1').hide();
    $('#validation_alert_step_1').text("");
    var email_address = $('#wt_new_customer_email_step_1').val();
    var email_validation = isEmail(email_address);
    if(!email_validation)
    {
      $('#validation_alert_step_1').text("Please enter a valid email address.");
      $('#validation_alert_div_step_1').show();
      return false;
    }
    if($.cookie("waitlist_customer_email") != email_address)
    {
      $.cookie("waitlist_customer_email", email_address );
    }
    
      $("#wt_popup_step_1").hide();
      $.cookie("wt_previous_step", '2' );
      $('#validation_alert_step_2').text("");
      $('#wt_submit_btn_step_2').show();
      $('#wt_popup_step_2').show();
  })

  $(document).on('click', '#wt_submit_btn_step_2' , function(e){

    var wt_regex_name = new RegExp('^([a-zA-Z ]){3,}$');
    if($.cookie("waitlist_customer_email") != null && $.cookie("waitlist_customer_email") != "" )
    {
      debugger;
      var device_width = $( window ).width();;
     
      var mob_web = "";
      if (device_width < 500)
      {
        mob_web= "Mobile Signup";
      }
      else
      {
        mob_web= "Web Signup";
      }

      $('#validation_alert_div_step_2').hide();
      $('#validation_alert_step_2').text("");
      var new_customer_firstname =  $('#wt_first_name_step_2').val();
      if(new_customer_firstname != "")
      {
        var new_customer_firstname =  camelize(new_customer_firstname);
      }
      
      var new_customer_lastname =  $('#wt_last_name_step_2').val();
      if(new_customer_lastname != "")
      {
        var new_customer_lastname =  camelize(new_customer_lastname);
      }
      debugger;
      var new_customer_birthday  =  $('#wt_birthday_step_2').val();
      var new_customer_birthday =  ((new_customer_birthday).split('-').join('/'));
      if(new_customer_birthday!==""){
        var get_accurate_date=new_customer_birthday.split('/')[1];
        var get_accurate_month=new_customer_birthday.split('/')[0];
      }
      var customer_birthdays=new Date(new_customer_birthday);
      var user_age=((Date.now() - customer_birthdays) / (31557600000));//calculate user age if negative it will never insert
      var birthday_year=  new_customer_birthday.slice(6, 10);
      var birthday_date=(customer_birthdays.getDate());
      var birthday_month=(parseInt(customer_birthdays.getMonth())+1);
      var total_days=daysInThisMonth(birthday_year,get_accurate_month);


      var new_customer_password  =  $('#wt_password_step_2').val();
      var new_passowrd_lenght    =  $('#wt_password_step_2').val().length;
      var new_dress_size         =  $('#wt_dress_size_step_2').val();
      var new_top_size           =  $('#wt_top_size_step_2').val();
      debugger;
      if(new_customer_firstname === '0' || !wt_regex_name.test(new_customer_firstname) ||  !wt_regex_name.test(new_customer_lastname) ||  new_customer_lastname === '0' || new_customer_password === "" || new_customer_birthday === "" || new_passowrd_lenght < 6 || new_dress_size == null || new_top_size == null || customer_birthdays == "Invalid Date" || birthday_year >= 2010 || birthday_year <= 1950 )
      {

        $('#validation_alert_div_step_2').show();
        $('#validation_alert_step_2').css({"color":"red"});
        if(new_customer_firstname === "0" || !wt_regex_name.test(new_customer_firstname))
        {
          $('#validation_alert_step_2').text("First name should be greater then 2 characters");
          return false;
        }

        if(new_customer_lastname === "0" || !wt_regex_name.test(new_customer_lastname))
        {
          $('#validation_alert_step_2').text("Last name should be greater then 2 characters");
          return false;
        }

        if(new_dress_size == null)
        {
          $('#validation_alert_step_2').text("Dress size is required");
          return false;
        }

        if(new_top_size == null)
        {
          $('#validation_alert_step_2').text("Top size is required");
          return false;
        }

        if(new_customer_birthday === "")
        {
          $('#validation_alert_step_2').text("Birthday is required");
          return false;
        }

        if(get_accurate_date>total_days){
          $('#validation_alert_step_2').text("Please enter a valid birthday");
          return false;
        }
        if(birthday_month > 12){
              $('#validation_alert_step_2').text("Please enter a valid birthday");
              return false;
        }
        if(birthday_date > 31){
              $('#validation_alert_step_2').text("Please enter a valid birthday");
              return false;
        }
        
        if(birthday_year <= 1950 || birthday_year >= 2010){
          $('#validation_alert_step_2').text("Please enter a valid birthday.");
          return false;
        }

        if(user_age < 10){
          $('#validation_alert_step_2').text("Please enter a valid birthday");
          return false;
        }

        if(user_age < 0){
          $('#validation_alert_step_2').text("Please enter a valid birthday");
          return false;
        }
      
        if(isNaN(birthday_month)){
            $('#validation_alert_step_2').text("Please enter a valid birthday");
            return false;
          }
        if(isNaN(birthday_date)){
              $('#validation_alert_step_2').text("Please enter a valid birthday");
              return false;
            }
        if(isNaN(birthday_year)){
          console.log(birthday_year);
            $('#validation_alert_step_2').text("Please enter a valid birthday");
            return false;
        }

        if(new_customer_password === "")
        {
          $('#validation_alert_step_2').text("Password is required");
          return false;
        }

        if(new_passowrd_lenght < 6)
        {
          $('#validation_alert_step_2').text("Password length should be minimum 6 characters");
          return false;
        }
        
        if (/\s/.test(new_customer_password)) {
          $('#validation_alert_step_2').text("Password should be without spaces.");
          return false;
        }
      }

      $('#validation_alert_div_step_2').hide();
      $('#loading_step_2').show();
      $('#wt_submit_btn_step_2').hide();
      FiltersSettings='FilterSettings:0';
      inviteZero ='invite0'; 

      
      var settings = {
        "url": "/admin/customers.json",
        "method": "POST",
        "headers": {
          "content-type": "application/json"
        },
        "data": {
          "customer": {
            "first_name": new_customer_firstname,
            "last_name": new_customer_lastname,
            "email": $.cookie("waitlist_customer_email"),
            "verified_email": true,
            "password": new_customer_password,
            "password_confirmation": new_customer_password,
            "send_email_welcome": true,
            "tags": "Birthday: " + new_customer_birthday + ",Dress Size : "+new_dress_size+",Shirt & Top: : "+new_top_size+", Profile IP: " + IP_ADDRESS+","+mob_web+","+FiltersSettings+","+inviteZero,
          }
        }
      };
      debugger;
      $.ajax({
        url: SHOPIFY_PROXY_URL + "generic_shopify_request.php",
        data: JSON.stringify(settings),
        dataType: "json",
        method: "POST"
      }).done(function (response) {
        response = JSON.parse(response);
        console.log(response);
        debugger;
        // if(response.errors != null && (response.errors.base != null ||  response.errors.email != null) &&  response.errors.base.length > 0) 
        // {
        //   taken_error = response.errors.base[0];
        //   name_email_missing = taken_error.includes("Customer must have a name or email address")? 1:0;
        //   email_already_taken = taken_error.includes("has already been taken")? 1: 0;
        //   is_invalid = taken_error.includes("is invalid")? 1: 0;
        //   if(name_email_missing) {
        //     $('#validation_alert_step_2').text("Name or Email Missing");
        //     return false;
        //   }
        //   else if (email_already_taken) {
        //     $('#validation_alert_step_2').text("Email has already been taken");
        //     $.cookie("waitlist_customer_email", '' );
        //     $.cookie("wt_previous_step", '1' );
        //     setTimeout(() => {
        //       $('#wt_popup_step_2').hide();
        //       $('#validation_alert_step_2').text("");
        //       $("#wt_popup_step_1").show();
        //     }, 1000);
        //   }
        //   else if(is_invalid){
        //     $('#validation_alert_step_2').text("Please enter a valid email address.");
        //     return false;
        //   }
        //   else {
        //     $('#validation_alert_step_2').text(taken_error);
        //     return false;
        //   }
          
        //   $('#loading_step_2').hide();
        //   $('#validation_alert_step_2').css({"color":"red"});
        //   $('#validation_alert_div_step_2').show();
        // }
        // else 
        if (response.errors != null && response.errors.email != null && response.errors.email.length > 0) {
          $('#loading_step_2').hide();
          $('#validation_alert_step_2').css({"color":"red"});
          $('#validation_alert_div_step_2').show();
          taken_error = response.errors.email[0];
          name_email_missing = taken_error.includes("Customer must have a name or email address")? 1:0;
          email_already_taken = taken_error.includes("has already been taken")? 1: 0;
          is_invalid= taken_error.includes("is invalid")? 1: 0;

          if(name_email_missing) {
            $('#validation_alert_step_2').text("Name or Email Missing");
          }
          else if (email_already_taken) {
            $('#validation_alert_step_2').text("Email has already been taken");
          }
          else if (is_invalid) {
            $('#validation_alert_step_2').text("Please enter a valid email address.");
          }
          else {
            $('#validation_alert_step_2').text(taken_error);
          }

          //$.cookie("waitlist_customer_email", '' );
            //$.cookie("wt_previous_step", '1' );
            // setTimeout(() => {
            //   $('#wt_popup_step_2').hide();
            //   $('#validation_alert_step_2').text("");
            //   $("#wt_popup_step_1").show();
            // }, 1000);
            return false;
        }
        else {
          var settings = { "email": $.cookie("waitlist_customer_email") };
          my_customer = create_customer_Stripe(settings,false,new_customer_firstname+"_ln_"+new_customer_lastname,new_customer_birthday,'','');
          $('#wt_popup_step_2').hide();
          $('#loading_step_2').hide();
          $('#wt_submit_btn_step_2').show();
          
          $.ajax({
            url: DBINFOURL + '/Waitlistcontroller/create_waitlist_customer',
            data: {
              'customer_shopify_id' : response.customer.id,
              'customer_stripe_id' : my_customer.id
            },
            dataType: "json",
            method: "POST"
          }).done(function (response) {
            if(response)
            {
              $("#wt_popup_step_2").hide();
              $.cookie("wt_previous_step", '3' );
              $('#validation_alert_step_3').text("");
              $('#wt_popup_step_3').show(); // didn't maked yet
              resettimer_step3();
            }
            
          })
        }
      });
    }
    else
    {
      $('#loading_step_2').hide();
      $('#validation_alert_step_2').css({"color":"red"});
      $('#wt_submit_btn_step_2').show();
      $('#validation_alert_step_2').text("Email Cookie not defined");
      $('#validation_alert_div_step_2').show();
    }
    
  })

  $(document).on('click', '#wt_submit_btn_step_3' , function(e){
    $('#validation_alert_div_step_3').hide();
    $('#validation_alert_step_3').text("");
    $("#wt_popup_step_3").hide();
    $.cookie("wt_previous_step", '4' );
    $('#validation_alert_step_4').text("");
    resettimer_step4();
    $('#wt_popup_step_4').show();
  })

  $(document).on('click', '#wt_socialite_btn_step_4' , function(e){
   
    $('#validation_alert_div_step_4').hide();
    $('#validation_alert_step_4').text("");
    $("#wt_popup_step_4").hide();
    $.cookie("wt_previous_step", '5' );
    $.cookie("wt_selected_plan", 'socialite' );
    $.cookie("wt_selected_plan_price", '79' );
    $('#wt_popup_step_5').show();
  })

  $(document).on('click', '#wt_trendsetter_btn_step_4' , function(e){
    $('#validation_alert_div_step_4').hide();
    $('#validation_alert_step_4').text("");
    $("#wt_popup_step_4").hide();
    $.cookie("wt_previous_step", '5' );
    $.cookie("wt_selected_plan", 'trendsetter' );
    $.cookie("wt_selected_plan_price", '109' );
    $('#wt_popup_step_5').show();
  })

  $(document).on('click', '#wt_wanderlust_btn_step_4' , function(e){
    $('#validation_alert_div_step_4').hide();
    $('#validation_alert_step_4').text("");
    $("#wt_popup_step_4").hide();
    $.cookie("wt_previous_step", '5' );
    $.cookie("wt_selected_plan", 'wanderlust' );
    $.cookie("wt_selected_plan_price", '139' );
    $('#wt_popup_step_5').show();
  })


  $(document).on('click', '#wt_back_step_2' , function(e){
    $('#validation_alert_div_step_2').hide();
    $('#validation_alert_step_2').text("");
    $("#wt_popup_step_2").hide();
    $.cookie("wt_previous_step", '1' );
    $.cookie("waitlist_customer_email", '' );
    $('#validation_alert_div_step_1').hide();
    $('#validation_alert_step_1').text("");
    $('#wt_popup_step_1').show();
  })

  $(document).on('click', '#wt_back_step_5' , function(e){
    $('#validation_alert_div_step_5').hide();
    $('#validation_alert_step_5').text("");
    $("#wt_popup_step_5").hide();
    $.cookie("wt_previous_step", '4' );
    $.cookie("wt_selected_plan", '' );
    $.cookie("wt_selected_plan_price", '' );
    $('#validation_alert_div_step_4').hide();
    $('#validation_alert_step_4').text("");
    resettimer_step4();
    $('#wt_popup_step_4').show();
  })

  $(document).on('click', '#wt_submit_btn_step_5' , function(e){
    $('#validation_alert_div_step_5').hide();
    $('#validation_alert_step_5').text("");
    $('#wt_submit_btn_step_5').hide();
    $('#loading_step_5').show();
    setTimeout(() => {
      handlewaitlistcheckout();
    }, 500);
})
  

$("#wt_popup_step_5").on('shown.bs.modal', function (e) {

  $('#validation_alert_step_5').text("");
  resettimer_step5();
  $("#plan_name_step_5").html("<b>" + camelize($.cookie("wt_selected_plan")) + "<b> <br>Rent 4 clothing items + 3 accessory at a time(unlimited swaps)<br>");
  $("#plan_value_step_5").html("Monthly Subscription(Cancel Anytime)  $" + $.cookie("wt_selected_plan_price") + ".00<br>");
  $("#priority_discount_step_5").html("Priority Access Discount  -$30.00<br>");
  $("#first_month_price_step_5").html("First Month Price  $" + ($.cookie("wt_selected_plan_price") - 30)+ ".00<br>");
  $("#due_now_step_5").html("Due Now  $0.00");
});




//end $(document).ready() below
})

//Ahmed Code Ends Here//


